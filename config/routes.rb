Rails.application.routes.draw do

  get 'pages/home'
  devise_for :users, controllers: { sessions: 'users/sessions' }
  devise_scope :user do
    get 'sign_in', to: 'users/sessions#new'
    delete 'sign_out', to: 'users/sessions#destroy'
  end
  root to: 'news#index'
  resources :news
  resources :agency_feeds
  resources :agencies
  resources :categories
  resources :users

  post 'save_click', to: 'news#save_click'
  get 'news_report', to: 'news#news_report'
  delete 'delete_all_news', to: 'news#delete_all_news'
  get 'fetch_all_news', to: 'news#fetch_all_news'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
