namespace :feeds do
  require 'rss'
  require 'open-uri'

  desc "Fetch News feeds"
  task news_feeds: :environment do
    News.fetch_feeds
  end
end
