require 'test_helper'

class AgencyFeedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @agency_feed = agency_feeds(:one)
  end

  test "should get index" do
    get agency_feeds_url
    assert_response :success
  end

  test "should get new" do
    get new_agency_feed_url
    assert_response :success
  end

  test "should create agency_feed" do
    assert_difference('AgencyFeed.count') do
      post agency_feeds_url, params: { agency_feed: { agency_feed_id: @agency_feed.agency_feed_id, agency_feed_url: @agency_feed.agency_feed_url, agency_id: @agency_feed.agency_id, category_id: @agency_feed.category_id } }
    end

    assert_redirected_to agency_feed_url(AgencyFeed.last)
  end

  test "should show agency_feed" do
    get agency_feed_url(@agency_feed)
    assert_response :success
  end

  test "should get edit" do
    get edit_agency_feed_url(@agency_feed)
    assert_response :success
  end

  test "should update agency_feed" do
    patch agency_feed_url(@agency_feed), params: { agency_feed: { agency_feed_id: @agency_feed.agency_feed_id, agency_feed_url: @agency_feed.agency_feed_url, agency_id: @agency_feed.agency_id, category_id: @agency_feed.category_id } }
    assert_redirected_to agency_feed_url(@agency_feed)
  end

  test "should destroy agency_feed" do
    assert_difference('AgencyFeed.count', -1) do
      delete agency_feed_url(@agency_feed)
    end

    assert_redirected_to agency_feeds_url
  end
end
