require "application_system_test_case"

class AgencyFeedsTest < ApplicationSystemTestCase
  setup do
    @agency_feed = agency_feeds(:one)
  end

  test "visiting the index" do
    visit agency_feeds_url
    assert_selector "h1", text: "Agency Feeds"
  end

  test "creating a Agency feed" do
    visit agency_feeds_url
    click_on "New Agency Feed"

    fill_in "Agency feed", with: @agency_feed.agency_feed_id
    fill_in "Agency feed url", with: @agency_feed.agency_feed_url
    fill_in "Agency", with: @agency_feed.agency_id
    fill_in "Category", with: @agency_feed.category_id
    click_on "Create Agency feed"

    assert_text "Agency feed was successfully created"
    click_on "Back"
  end

  test "updating a Agency feed" do
    visit agency_feeds_url
    click_on "Edit", match: :first

    fill_in "Agency feed", with: @agency_feed.agency_feed_id
    fill_in "Agency feed url", with: @agency_feed.agency_feed_url
    fill_in "Agency", with: @agency_feed.agency_id
    fill_in "Category", with: @agency_feed.category_id
    click_on "Update Agency feed"

    assert_text "Agency feed was successfully updated"
    click_on "Back"
  end

  test "destroying a Agency feed" do
    visit agency_feeds_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Agency feed was successfully destroyed"
  end
end
