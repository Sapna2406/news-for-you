class NewsController < ApplicationController
  before_action :set_news, only: [:show, :edit, :update, :destroy, :save_click]
  before_action :authenticate_user!, only: [:show, :edit, :update, :destroy, :create, :updated, :news_report, :delete_all_news, :fetch_all_news]
  skip_before_action :authenticate_user!, only: [:save_click, :index]

  # GET /news
  # GET /news.json
  def index
    @category_ids = params[:categories]
    
    # Call this to fetch news from rss sources and store into db.
    # Uncomment "News.fetch_feeds" if background scheduling does not work.
    # News.fetch_feeds
    ##################

    @categories = Category.all
    @all_news = News.get_news @category_ids
  end

  # GET /news/1
  # GET /news/1.json
  def show
  end

  # GET /news/new
  def new
    @news = News.new
  end

  # GET /news/1/edit
  def edit
  end

  # POST /news
  # POST /news.json
  def create
    @news = News.new(news_params)

    respond_to do |format|
      if @news.save
        format.html { redirect_to @news, notice: 'News was successfully created.' }
        format.json { render :show, status: :created, location: @news }
      else
        format.html { render :new }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /news/1
  # PATCH/PUT /news/1.json
  def update
    respond_to do |format|
      if @news.update(news_params)
        format.html { redirect_to @news, notice: 'News was successfully updated.' }
        format.json { render :show, status: :ok, location: @news }
      else
        format.html { render :edit }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /news/1
  # DELETE /news/1.json
  def destroy
    @news.destroy
    respond_to do |format|
      format.html { redirect_to news_index_url, notice: 'News was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # DELETE /news/1
  # DELETE /news/1.json
  def save_click
    puts params[:id]
    if @news.present?
      count =  @news.click_count.to_i + 1
      @news.update(click_count: count)
    end

    respond_to do |format|
      format.js
    end
  end

  # Method to manage news and view click report
  def news_report
    @all_news = News.order(click_count: :desc)
    @show_report = true
    render :template => 'news/index'
  end

  # Delete all news
  def delete_all_news
    News.delete_all
    @show_report = true
    @all_news = News.all
    render :template => 'news/index'
  end

  def fetch_all_news
    News.fetch_feeds
    @show_report = true
    @all_news = News.all
    render :template => 'news/index'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_news
      @news = News.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def news_params
      params.require(:news).permit(:news_title, :news_description, :news_publish_date_time, :news_link, :click_count, :category_id, :agency_id)
    end
end
