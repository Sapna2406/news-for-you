class AgencyFeedsController < ApplicationController
  before_action :set_agency_feed, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /agency_feeds
  # GET /agency_feeds.json
  def index
    @agency_feeds = AgencyFeed.all
  end

  # GET /agency_feeds/1
  # GET /agency_feeds/1.json
  def show
  end

  # GET /agency_feeds/new
  def new
    @agency_feed = AgencyFeed.new
  end

  # GET /agency_feeds/1/edit
  def edit
  end

  # POST /agency_feeds
  # POST /agency_feeds.json
  def create
    @agency_feed = AgencyFeed.new(agency_feed_params)

    respond_to do |format|
      if @agency_feed.save
        format.html { redirect_to @agency_feed, notice: 'Agency feed was successfully created.' }
        format.json { render :show, status: :created, location: @agency_feed }
      else
        format.html { render :new }
        format.json { render json: @agency_feed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /agency_feeds/1
  # PATCH/PUT /agency_feeds/1.json
  def update
    respond_to do |format|
      if @agency_feed.update(agency_feed_params)
        format.html { redirect_to @agency_feed, notice: 'Agency feed was successfully updated.' }
        format.json { render :show, status: :ok, location: @agency_feed }
      else
        format.html { render :edit }
        format.json { render json: @agency_feed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /agency_feeds/1
  # DELETE /agency_feeds/1.json
  def destroy
    @agency_feed.destroy
    respond_to do |format|
      format.html { redirect_to agency_feeds_url, notice: 'Agency feed was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_agency_feed
      @agency_feed = AgencyFeed.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def agency_feed_params
      params.require(:agency_feed).permit(:agency_feed_url, :agency_id, :category_id)
    end
end
