json.extract! agency_feed, :id, :agency_feed_url, :agency_id, :category_id, :created_at, :updated_at
json.url agency_feed_url(agency_feed, format: :json)
