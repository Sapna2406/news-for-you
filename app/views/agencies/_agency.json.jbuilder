json.extract! agency, :id, :agency_name, :agency_logo_path, :created_at, :updated_at
json.url agency_url(agency, format: :json)
