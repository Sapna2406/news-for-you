json.extract! news, :id, :news_title, :news_description, :news_publish_date_time, :news_link, :click_count, :category_id, :agency_id, :created_at, :updated_at
json.url news_url(news, format: :json)
