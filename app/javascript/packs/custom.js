/* Save click counts when click on news link */
$(".news-link").on('click', function() { 
  $.ajax({
    type: "POST",
    url: "/save_click",
    data: {id: $(this).closest('.news-item').attr('id')},
    success: function(resultData) { console.log("CLick count saved") }
  });
});
