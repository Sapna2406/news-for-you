class Category < ApplicationRecord
  has_many :news
  has_many :agency_feeds
end
