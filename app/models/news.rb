require 'rss'
require 'open-uri'

class News < ApplicationRecord
  belongs_to :category
  belongs_to :agency
	validates :news_link, uniqueness: 
	
	def self.fetch_feeds
    categories = Category.all

    categories.each do |cat|
      cat.agency_feeds.each do |agency_feed|
        agency_feed_url = agency_feed.agency_feed_url
        news_rss = RSS::Parser.parse(open(agency_feed_url).read, false).items       
        news_rss.each do |feed|
          # pub_date = DateTime.parse('#{feed.pubDate}')
          News.create(news_title: feed.title, news_description: feed.description, news_publish_date_time: feed.pubDate, news_link: feed.link, click_count: 0,category_id: cat.id, agency_id: agency_feed.agency_id)
        end
      end
    end  
  end
  
  def self.get_news categories_ids = []
    if categories_ids && categories_ids.length > 0
      News.where("category_id in (#{categories_ids.join(',')})").order(news_publish_date_time: :desc)
    else
      News.order(news_publish_date_time: :desc)
    end
  end

end
