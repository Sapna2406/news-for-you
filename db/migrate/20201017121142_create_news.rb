class CreateNews < ActiveRecord::Migration[6.0]
  def change
    create_table :news do |t|
      t.integer :news_id
      t.string :news_title
      t.text :news_description
      t.datetime :news_publish_date_time
      t.text :news_link, unique: true
      t.integer :click_count
      t.integer :category_id
      t.integer :agency_id

      t.timestamps
    end
  end
end
