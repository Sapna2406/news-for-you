class CreateAgencies < ActiveRecord::Migration[6.0]
  def change
    create_table :agencies do |t|
      t.integer :agency_id
      t.string :agency_name
      t.text :agency_logo_path

      t.timestamps
    end
  end
end
