class CreateAgencyFeeds < ActiveRecord::Migration[6.0]
  def change
    create_table :agency_feeds do |t|
      t.integer :agency_feed_id
      t.text :agency_feed_url
      t.integer :agency_id
      t.integer :category_id

      t.timestamps
    end
  end
end
