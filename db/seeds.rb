# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


ActiveRecord::Base.connection.execute("TRUNCATE users RESTART IDENTITY")
User.create(name: "nfuadmin", email: "admin@nfy.com", password: "nfuadmin")

ActiveRecord::Base.connection.execute("TRUNCATE categories RESTART IDENTITY")
categories = ["Sports", "Business", "Entertainment", "Science", "Health"]

categories.each do |category|
	Category.create(category_title: "#{category}")
end

ActiveRecord::Base.connection.execute("TRUNCATE agencies RESTART IDENTITY")
agencies = ["Times Of India", "Hindustan Times", "The Hindu"]

agencies.each do |agency|
	Agency.create(agency_name: "#{agency}")
end

ActiveRecord::Base.connection.execute("TRUNCATE agency_feeds RESTART IDENTITY")

AgencyFeed.create(agency_feed_url: "https://timesofindia.indiatimes.com/rssfeeds/4719148.cms", agency_id: Agency.first.id, category_id: Category.first.id)
AgencyFeed.create(agency_feed_url: "https://www.hindustantimes.com/rss/sports/rssfeed.xml", agency_id: Agency.second.id, category_id: Category.first.id)
AgencyFeed.create(agency_feed_url: "https://www.thehindu.com/sport/feeder/default.rss", agency_id: Agency.third.id, category_id: Category.first.id)

AgencyFeed.create(agency_feed_url: "https://timesofindia.indiatimes.com/rssfeeds/1898055.cms", agency_id: Agency.first.id, category_id: Category.second.id)
AgencyFeed.create(agency_feed_url: "https://www.hindustantimes.com/rss/business/rssfeed.xml", agency_id: Agency.second.id, category_id: Category.second.id)
AgencyFeed.create(agency_feed_url: "https://www.thehindu.com/business/feeder/default.rss", agency_id: Agency.third.id, category_id: Category.second.id)

AgencyFeed.create(agency_feed_url: "https://timesofindia.indiatimes.com/rssfeeds/1081479906.cms", agency_id: Agency.first.id, category_id: Category.third.id)
AgencyFeed.create(agency_feed_url: "https://www.hindustantimes.com/rss/entertainment/rssfeed.xml", agency_id: Agency.second.id, category_id: Category.third.id)
AgencyFeed.create(agency_feed_url: "https://www.thehindu.com/entertainment/feeder/default.rss", agency_id: Agency.third.id, category_id: Category.third.id)

ActiveRecord::Base.connection.execute("TRUNCATE news RESTART IDENTITY")

