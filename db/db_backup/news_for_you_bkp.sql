PGDMP         #            	    x            news_for_you #   12.4 (Ubuntu 12.4-0ubuntu0.20.04.1)    13.0 /    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    24577    news_for_you    DATABASE     ]   CREATE DATABASE news_for_you WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'C.UTF-8';
    DROP DATABASE news_for_you;
             
   newsforyou    false            �            1259    57440    agencies    TABLE     �   CREATE TABLE public.agencies (
    id bigint NOT NULL,
    agency_id integer,
    agency_name character varying,
    agency_logo_path text,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);
    DROP TABLE public.agencies;
       public         heap 
   newsforyou    false            �            1259    57438    agencies_id_seq    SEQUENCE     x   CREATE SEQUENCE public.agencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.agencies_id_seq;
       public       
   newsforyou    false    209            �           0    0    agencies_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.agencies_id_seq OWNED BY public.agencies.id;
          public       
   newsforyou    false    208            �            1259    57451    agency_feeds    TABLE       CREATE TABLE public.agency_feeds (
    id bigint NOT NULL,
    agency_feed_id integer,
    agency_feed_url text,
    agency_id integer,
    category_id integer,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);
     DROP TABLE public.agency_feeds;
       public         heap 
   newsforyou    false            �            1259    57449    agency_feeds_id_seq    SEQUENCE     |   CREATE SEQUENCE public.agency_feeds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.agency_feeds_id_seq;
       public       
   newsforyou    false    211            �           0    0    agency_feeds_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.agency_feeds_id_seq OWNED BY public.agency_feeds.id;
          public       
   newsforyou    false    210            �            1259    57352    ar_internal_metadata    TABLE     �   CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);
 (   DROP TABLE public.ar_internal_metadata;
       public         heap 
   newsforyou    false            �            1259    57429 
   categories    TABLE     �   CREATE TABLE public.categories (
    id bigint NOT NULL,
    category_id integer,
    category_title character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);
    DROP TABLE public.categories;
       public         heap 
   newsforyou    false            �            1259    57427    categories_id_seq    SEQUENCE     z   CREATE SEQUENCE public.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.categories_id_seq;
       public       
   newsforyou    false    207            �           0    0    categories_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;
          public       
   newsforyou    false    206            �            1259    57462    news    TABLE     �  CREATE TABLE public.news (
    id bigint NOT NULL,
    news_id integer,
    news_title character varying,
    news_description text,
    news_publish_date_time timestamp without time zone,
    news_link text,
    click_count integer,
    category_id integer,
    agency_id integer,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);
    DROP TABLE public.news;
       public         heap 
   newsforyou    false            �            1259    57460    news_id_seq    SEQUENCE     t   CREATE SEQUENCE public.news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.news_id_seq;
       public       
   newsforyou    false    213            �           0    0    news_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.news_id_seq OWNED BY public.news.id;
          public       
   newsforyou    false    212            �            1259    57344    schema_migrations    TABLE     R   CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);
 %   DROP TABLE public.schema_migrations;
       public         heap 
   newsforyou    false            �            1259    57418    users    TABLE     �  CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);
    DROP TABLE public.users;
       public         heap 
   newsforyou    false            �            1259    57416    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       
   newsforyou    false    205            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public       
   newsforyou    false    204                       2604    57443    agencies id    DEFAULT     j   ALTER TABLE ONLY public.agencies ALTER COLUMN id SET DEFAULT nextval('public.agencies_id_seq'::regclass);
 :   ALTER TABLE public.agencies ALTER COLUMN id DROP DEFAULT;
       public       
   newsforyou    false    209    208    209                       2604    57454    agency_feeds id    DEFAULT     r   ALTER TABLE ONLY public.agency_feeds ALTER COLUMN id SET DEFAULT nextval('public.agency_feeds_id_seq'::regclass);
 >   ALTER TABLE public.agency_feeds ALTER COLUMN id DROP DEFAULT;
       public       
   newsforyou    false    211    210    211                       2604    57432    categories id    DEFAULT     n   ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);
 <   ALTER TABLE public.categories ALTER COLUMN id DROP DEFAULT;
       public       
   newsforyou    false    207    206    207                       2604    57465    news id    DEFAULT     b   ALTER TABLE ONLY public.news ALTER COLUMN id SET DEFAULT nextval('public.news_id_seq'::regclass);
 6   ALTER TABLE public.news ALTER COLUMN id DROP DEFAULT;
       public       
   newsforyou    false    212    213    213                       2604    57421    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       
   newsforyou    false    205    204    205            �          0    57440    agencies 
   TABLE DATA           h   COPY public.agencies (id, agency_id, agency_name, agency_logo_path, created_at, updated_at) FROM stdin;
    public       
   newsforyou    false    209   �6       �          0    57451    agency_feeds 
   TABLE DATA           {   COPY public.agency_feeds (id, agency_feed_id, agency_feed_url, agency_id, category_id, created_at, updated_at) FROM stdin;
    public       
   newsforyou    false    211   �6       �          0    57352    ar_internal_metadata 
   TABLE DATA           R   COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
    public       
   newsforyou    false    203   8       �          0    57429 
   categories 
   TABLE DATA           ]   COPY public.categories (id, category_id, category_title, created_at, updated_at) FROM stdin;
    public       
   newsforyou    false    207   Z8       �          0    57462    news 
   TABLE DATA           �   COPY public.news (id, news_id, news_title, news_description, news_publish_date_time, news_link, click_count, category_id, agency_id, created_at, updated_at) FROM stdin;
    public       
   newsforyou    false    213   �8       �          0    57344    schema_migrations 
   TABLE DATA           4   COPY public.schema_migrations (version) FROM stdin;
    public       
   newsforyou    false    202   K      �          0    57418    users 
   TABLE DATA           �   COPY public.users (id, name, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, created_at, updated_at) FROM stdin;
    public       
   newsforyou    false    205   �      �           0    0    agencies_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.agencies_id_seq', 4, true);
          public       
   newsforyou    false    208            �           0    0    agency_feeds_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.agency_feeds_id_seq', 7, true);
          public       
   newsforyou    false    210            �           0    0    categories_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.categories_id_seq', 6, true);
          public       
   newsforyou    false    206            �           0    0    news_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.news_id_seq', 1831, true);
          public       
   newsforyou    false    212            �           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 1, true);
          public       
   newsforyou    false    204            %           2606    57448    agencies agencies_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.agencies
    ADD CONSTRAINT agencies_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.agencies DROP CONSTRAINT agencies_pkey;
       public         
   newsforyou    false    209            '           2606    57459    agency_feeds agency_feeds_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.agency_feeds
    ADD CONSTRAINT agency_feeds_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.agency_feeds DROP CONSTRAINT agency_feeds_pkey;
       public         
   newsforyou    false    211                       2606    57359 .   ar_internal_metadata ar_internal_metadata_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);
 X   ALTER TABLE ONLY public.ar_internal_metadata DROP CONSTRAINT ar_internal_metadata_pkey;
       public         
   newsforyou    false    203            #           2606    57437    categories categories_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.categories DROP CONSTRAINT categories_pkey;
       public         
   newsforyou    false    207            )           2606    57470    news news_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.news DROP CONSTRAINT news_pkey;
       public         
   newsforyou    false    213                       2606    57351 (   schema_migrations schema_migrations_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);
 R   ALTER TABLE ONLY public.schema_migrations DROP CONSTRAINT schema_migrations_pkey;
       public         
   newsforyou    false    202            !           2606    57426    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         
   newsforyou    false    205                       1259    57473    index_users_on_email    INDEX     N   CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);
 (   DROP INDEX public.index_users_on_email;
       public         
   newsforyou    false    205                       1259    57474 #   index_users_on_reset_password_token    INDEX     l   CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);
 7   DROP INDEX public.index_users_on_reset_password_token;
       public         
   newsforyou    false    205            �   f   x�3�����M-V�OS��K�L�����������[��2i���K)-.I�S ��4SKSc<R\�`�e�*�M�m����))�=... �I2!      �   �   x���]n� �gXE7P�򯋘̋3b4u����:m3�J�pC'�K@�'��xǒ���Ǧ�b�ް���	�	�F��=��H -)�x�4�@)��H'3���,�Mތ�~����Kgo���#�i�\&�j��6��ʭ�`����T�-�uz+��DT��]���|�{Paa�����?�{��n��g�(@�����l���n��|&�.k�!�)V�Ч�>�����(���o�"zf��w���F      �   H   x�K�+�,���M�+�,I-.�4202�5"Ks+C+cC=3#cSd)CS++S=C3c3K�=... 'q      �   �   x�}��
�0g�)���u,2u�%A�b��{��vǁ�bz����8%���pA��}���wA� P�p��(7�J�&�KaHL�1}(�lQ�F�"��1K��P�sں�$F���:�z��GE0u ���Ho      �      x�Խ��F�.�[�8�=S�YF�S�},3KU���j2K����A$Q
�̢���+���5�c��/��Q�$�{.L��jUwל9�-%H"�=�ï�����ſ������WN�:ʜ0ϜEm�(�Ul�E�:^��9��a;_�[�o����n��<ԑ���ȃ��i���%�6�q��;���"���]��N��4s��t�c��J�'�u��B��&�q��M�уܹ	��ʹ��E���/�>����;n��7>�_��|��=~|www���U���<�<�<Hs���Ǜ�6���b�Q��h�@eh�h��ah�X�y8���7���4�_������K�:��l�����Ao2�y�;w�����W�\ǿ���N�AJ4"*ܭv��q��IN�r'��8X�<��4����9�\Ϝ�Ux��L�a+��-�R��+������yN�����ټ�2b�*̜bK�#68w��ɂ�膟z����~��,��3�w�|�Sz���W:wV��I�	b���Ep���kN�p�i�;�c�l0>x�cE��UEVEdU�FMh��XY�*��� ��J�48��]�z����#p~ ο&
�7�g��7i�q��wA�>c:D����WI��4Xw�G��_"�eBT�!F�,+@��p�՛0��$J�;g�&|�=��Oo �$�&j��^�Kv����sJ""��]BG��Y%iJ���0��s͌�_�$tz�O�p���?yVm��㣏�8�F%��LT�Qާ�>�e���A՚	�V�����*%�	�Xg|8�w1~䍦�#�~�'S0��E�&)6a�j��v�>��$w���3Ob�}I`�<��􇹎�d$d~�_����[��OH�$$��!kH��0��ϓ��p��'���]H��� ��$�KaMGL�;�>�I�N�Wi��	H{��[gR�+�'�N�������x5�V��ќ~���v�'��VI�s�Z�������C�G�K�_�&Y�
����?^=!�>7�ot6~�aJ���D�K�dJ�J�(f���P�	e8Q;2��t�v��h��Gё���-�d����y���չLb,X;O�4��PԤu�㎳!���#�HR����lvt��9;xIJ�0G(NH�Y@���Xd�:�ĵ��>Dl#b;�r�tw$�H鰌j��B�#��AL�*(PA�\���L��.���#�'?_\��e2_��	�*��p��`�v�"��ٹ��EÈ������\��4퓞���Z��*�����ID� �d=��$�eD��o���C<6���2~c��7�����| @��私�O*��j�%��Y���Ԡ����Ҫ��'$��;8s��ndH���*O�R)��3��
���P��(���J(�dv����x2�yf������Ml�sC�.6$}y��}�~D��>I^�O�R��p�����W�6���V�Bq�5z¼e�|A40��O�F�����h�Auꌵ'�jt ���l��[�]�<������Ȍ	�2Fe�0�	�,a����M�T�GD���^��nܮ2�	��v5T�U�Ŀ�Ic�䣀h���$���g��rj��֕ݮ_8/�����O��I!�&���L�7����4�aC��l
�6��ek*ƴwg�:̑���μ��"z�cz�A��U� ��[W�u�%[Wf��t�"�z�3�i��3��n��#��-�G���#�Q�H���XЭs��d	�kA�9��(߲品�),��A�,�����d��t8"W)={Y���A���vΣ�;�6��Nw�U2���i�#LȞ$�{T�z�N>R��٥d�2�H$�@�+��O�ȅ�JuVz�S����䀔�#�rV�#7�t�GI�Y�?����le�/�0��c�@G������ä�Hfğ�pC�qV�EW:����Ńz�&���.B(�e@?��+D�*~Ҝ�b�Y\�q-��c�ڛ�y�D��l��%�3����l��!�9526��v�w0�U~����[i�n�؄�NV.n���y����jhN��u�/��d�5���ҹ4]t��xI2N�\��O���6"��ъ���¹s�AR	!%�U�"ɢ���dXV��Bj��O�^:n8=���}�X0H�p	]�cJ)!�k,d�ȃ!S�{�p8��ސ��v�E�{cp�"���O|~	?��eWĥCXoEN��"]o`N8��vX�-Hr�����A@(Z�v��e!#�?��{�&w�E&z�Xb����|t�5�x��R�buL��\,`m����(��Mܣ��۳ޑ�A�e�I���MAT
ϘXj5W�X�A,e��f;�]� �Z�X|o�X��X����q�yG�����A!�#J�d�A��pl�,�SO�'��o����?0eɎ�ڍP���n����Zf����s�8����^��OB��z�_�3�N��z�G����?�1�W��A����F�QA�!R��(
�!D�-�|�)�`O��N��Q�?(��	��d��;��w�Ή��r��i��I���!�,� .�H�^i�e?�;�%����?�8�{��ɏ["�غ�I��F��>
~2��!3�zәC��9׉��Is�d��>6�Bd o���>�)&�Z���P���>(��&o�IWحG��y�x+�P^��Cl����Q%9��B>�Ȁ:`���_YZ䡣}b�T�$��=5;D?=�l���*$���%ӳ��4�X�:zL��3�״,E�R�+E�V�+��H����3��Ag8�{�ʏ@Wר$�0N�"L��zϜ��t�ܐiUl5Y3������dI���)&V�*�<�+�_�٩d���^�i$Cn$�Ґ�����I��A=K�\�2�+��a�t�!PėTeA:|vI��;������Q/��՛�NN�R���Ul���i�@Cu�WmI3�IBE|�	�+6x�M;eț�&B���fs�9�=ć�'4*�R$�DBI]�|�x-�����^��&i�:K9o���7ȉ#��Z�~��y�Pp��M��m�8�����6�E8ya�R'>��1��>'���P'{��	�ٞ@�|�mZl�8��'�Z;�n��?�Ч�s�3^t7�v68��)��.z�7��iIJ�L̽;����?��&�jm)AJ���|ܑ��х�!��XCW�f�Ё�.H���*�LY.�n5Tj4R �~�9�{�c�~�F|�.o^8��s}�q�3:7�"�0$ ��2]fA됼@D�_��F����vh�?|iim�%|���_�|ٖN�{�!���q�lٯ���3�L�)�� ��V*KRfIjU�9B�Xyf[�iX�ۧ�)`7=Oƃ�t��7ٗ�}�Y:�$%��_��tV�Vσ?|��?��.�?|��K2�w�}�S���bٞ���1��"{v6�?����v�]%y�\�������xɁ��mؾ��'o�$��uNۉsc�����~����{91���|�x,��!���s�����l�H����F�Ƨ=o�o���G8��O��q���$fb���3قK��9���VmX��1���oHƱe�`�Y�d�)���%�F�O�e��n�l�w���x�S_�i�����{��#~�M����5��G��m"R��@�ӝV�I�"�{ak��DJ�	L,���y�~���>�2��"8%;��y��K�NVaNζN7ڑs��M��E!��3�tl�$9�����'�D�Vex�+w�~��z���'�!B@�/uM`�c����"!��oNF_������:�|N�1�����@g��;��>2����lsnn���c�F���X"�"�_>��F� $7y
�i��W�~����A�>����&Њ��{�nB���#��r�H�`�/��ؤ���4��~1'���w�8����9�k$�_�X�s�8����92����|G�3��F(����o�p^q�GE$Z}����L<d�E�|+����w�d� �y�pY�r��C�,�b��!�Dd��Ĳ�!r���a�d9a<    �
(*����)�6�9�A7��q(ߓ¢O�i��!h�O��7N%%':���;�,�����d֝yާ����>��y]�A8KI����8k�VaY���z $�:6��:�M�Y�,�͊�c>c��6��]�Jy5�r��ҳ�������}6~�L�|TB���N�Q�_�����N[�4�#ȩ9�t �͖C�l �DVz������-5	��"�����2H77QX����e"uL����B�	�x�?��W.����Eg��mţ��f�-�$�I��Թ���wu�s�|�d��6�2�:1j��Z�o^�����l�"����A��:���{R�I��@v�ѳy�r�b������M����U2�(P��uZ��S�~��i��؏vP�h*�6�����3�����D��jΥ~z#og3)c���!% ���ʂ�o��OU�r�8L>���w9�Ǥ~2��z~DR?�I0�kUf�eS\�U�����ĉ�O���C���܀p�I�GW�,P傪+�S0wb�+�Gj�h=
�Q��оZӺ��7�|tA����*t.5��f��f!*�sr�4I�+�9&:��Wߑ���7�6��G����]r�t��3sM��Q�Yť��e�i�qb
zI'̃,[O?XH6��%��y�('n��5E�C�
�G��#��F�IKX�|��Tƣ��������?:3�b��J�'�.t����Z&9���;1�wI��)X��J�edԸˢ/�i�Eݣ�כ|F��j9��5�D�����tM�Pu��dx��� ��9�Ek�G4�3+�~�k%�L�>Υ>̆=���U�|ۄD�<��cc�]���,<���8��t#�>ɍ��>#�d�s{m�C�#�W�>K��s��9�_�]mR���#6�9W��$i(�^%i����o$N�.�j&�U�j��T�|N�ƾQ���go�9E@��܏�pJ�Ac��˚#�$u����#��0�v�3���&��v��G�ov.���f��̯��_����#����6�"�V��j�~̹��ZL�@>Ʃ�p�x�K���ߞGI�6�")R�ь6rgR{7�c� �$�1��:
H���r1gm���]A�{��َ��l�7%$>�!<&���;n�U�#�!�A��d���H⧡OB�$�	���2N���!:F;����<��<.�$G~�1y��b���
�fU���Ueda�r/�Kx��X꒷��T�7�}>RZ-�H�q���e�i�E-���{T�d���e痗�%RmΏ!��	E�S�n5�@�g�/Y�����7㉳ML������"Jp%J������~d�Z���IK�wh�8&{�3<�����H�?����78t��#�׮�B�� �'M���.QBR�ED>疖�d���~q+[���u�KQX
b�s,EՖB$�HG��4��k��{���M��I��r�`�\:u�W�_u��li/��p.�!L?�q.�|�`�wz���2b��!?ЕE���.(-Ȩ�X
3��f���SC�������tF!{�ڰ�1�=��IZl����rq,'�4��X�����,��|i��q��l5��B�X�����Kf;��ޡIg�Y;�W�M��q>�I�ě��2�� �\�r��
xm6̃H
���&�(��X�_[r�]���"�un���)W�������"T���r%
+Qb�(�W��E�T�X�}��}V�#�|���,��[�O���NL�(���m��,b��z2�4W���or{��_=�\p�H�ٞh����|�v��293י��lK��s9��Dza�{�݌Ee�U����f:X��h�к�
�ؠ�c���'9�y�'��#�I>����'�N.��Nh-;��
��$8�Ὃ�e��=��e���o�}�,��(��
1b�ܒ&s�
�Q��ňE"�	�񱘖��;����.�h��'�qށ�\��N���1���G%f�xо��/�U��gvm$�-T�%���=�G�D�8�Tĵ��.鶔t�6��|��A���3����G������x����'��K���n~��879&�*+1�1y�{t^�h �[JN�u�>|_�滌K����i1ܧ��b�L
��;Q�E����5�B'k��{�QL�Z���8c4��������Q��q���\jB&r��['K� �D!I@����z BN"�ks���2�Ǹ�d�k0��/��
"�%^� ܨ����z�8�G;����6��������9&h�Ѵ��MW>��q����e�9�>-P�-O�*Yd�7�g���]�l�$b2��(��Vd�*qK�0����b�X��*�+�{۬P��j�He���\�}�5�?#,�c#.�)���?K���@ٖ4CT/�+}G�ec^g	���l��cϕs��X\M)�br� �8cu�����	0�i9y�Ǭ���|'�"�/1͚�ɷ'�-�5؄s�-���YF��\�j�>��ip� �S�_�EJ�g���*uc(Ӌ�ti�<��7O�.Wo|�}���Y��cz`�M�~=��6o�����W�oBr9nt������K����]` �~{^C�;H��+^�|��j��lE�<Tؚũ�T����g�%�i�7���pW˱��f��L���3r�"�B��6��%1�]�$C�G�a%�0<��(U�Yű��&����D�3@��k�2e;wAp@�ç�J�瀮G�SXI�}Y��`Y���ɕ�cy=��gé�Q�B��,���M�,���sL��cor@���Մ�Y��l��Ǣ�\�U!MH����I?�Ii�� ˊ�k#v���N<��yxm֢�Y�"�!0�֑J��Lam� ]j}�!��]$�v��8 ~F�W.���Ss�9,����;��/6R�}RV��ջ*���"�$l9b����A]$7�(%|:��&b�%�XI��g'N�	�h�+�V ����9gW�D�M��3���y�R]�R��g��Զଢ� �1x�S�>S_��OWo��I��/�e����8�w����R��$����,����H1��J��A=&�Sw�ko��G�t���"�%�b���W!Bӏ8z��|g�G&)f�8��T98N��lh�=^
�?ߏ ceV�hA�lBbe��Qi���&�Ŕ���>��J���w�B�r?v��2i�`Zn^}��:}��ӳX�:�(�q|L�/ԎJ��{�@�����,!�8t���J��j7���X7la剻;�K)U^D@H],�c��l��TƚF�1I'5�1��9�����d�:Nء���B
��~#��\�'�=���Q7�?����~o��Gb���9�6��e�d�r�����s@jwI�9�GC��~�Bn�p4�\� ����q����]2�f;�}��Y��zcP���,tf,2*�k�ٲ+��X��@����H�k���qGn���;Y'��Ĝ��u��@b,袰���E�+�Y��5��ƕ[L6e��H%��%�m�؇���c{#�^t4���c����V����B���T��b���e(�Y��r���&�|R㋴C>�jiyy9�,-��R���c5j��ܓ��7��*F��Տ%!,�թ��Η{���D�0v�1�h60�p^�V@�D4���'��3�+�|���#���izA���H�q�u�*�Z5r��/�C�8 >�-�S���� ʹl�em �����R�*�Η:E��l��;�m�C!��|��M_4X���t��tO�Gg�'�����cjܝ��c� �}1��嘻-`^������ľLF�xP�?*�߃�o��8�K���[m� �#ל}��S/.��A6����ܣ�7��jW����T�Ϥ ($�I&a��Ğ�Z�8w�H�?��q��4���:b�A���dS}	��H��#TM���]��,�E�nig#?�q�g�K���n4+N�S�$�jS.�c.�1�Ls��ϔio �$��V�|�GM��/��:<c0ԿIR�a�z��=�'� �\c�G�H�/�H�p^�M�Qb���O:��i��w#X_qI+��8S�&    $k�e)6�A�[��5��_S,VO�1	)Oż��*o:�����r�?Gw4�5�.m�����0K�kt��!���à�i�vk˙�a[��:����'��o#�����d�`������3GԱ@ȴ��-�Xʾ���>�;ʥ,�n��p�"��S��*�Jʭ�ˁo�fz>8j��(i�>�s��U>j~�H�5�IҐƂ/M�οm
���>	��?���i+k�ڀ���d�"�aJ'P�h����Nr����d���kG�:�KR�_<)ȲC��|��W�H�t��U��'�d@� k̺��xu�V_︽���C p��������U� ]3n�܂��C[&:2S�#	�Q$�������w��;�޺�������h�,��Sh���-���_[(�X�����џ������x�����ӛ��s��73��g4�ڀ"�G`�H�"�P�W���lZ�0�����6�n�9��`����&���!�'��ɻ
�%[�m�� ܸ�C�Te��99[I�H@�2'�#T�A��?G�;�?�s�^[���	�	���� J$��	VT�LMa��|���|�1	�hw�	2�xD �Aͅt,ǹ ��!����y�����śI��O�k����.���h�ߔG�.�B�v���;�YӿX������U�32�ȗ�D�}��c�F�9���-�Eb��i�*�# s�y��1�,��c�]���f`D�(m�Y�,/��}��Θm��RҘ$r-+�D� �u/^�̶��+�'�W��6�.�Of��~���_zW������<����-
QN'(��L]\�e���
����gc��z��eZ�,�UO�h|ϙz��L N�����AMa�QV[�_;q������
gѮ\�����(̹���RV��am�6Rڼ�+t�}�����35�kU%ݐD�g�V�$���(Zڡ���bX�|;����������ް�އ�NEK��2Ry�N���{��u��E�1T)�n�3ȯ��#I�&b�m/�<�䎫zh��1��%���J�Vb[sߚ�a��v!ܪ����f���z��n��<Ӌu�$�f�tJD,R���5�r8bN}5��b��G�,�ˠ���� Y��ٌ�%ڕbڑYeh��v���~9�]��w����'���O��:y<�&�����2�ۈ̘8�p�!�q:_�
���n��Y�z�\c�]���x�'.HrOOO���:�X���k�+�(�;燛'�dvs���)1�'��s�9���9]����%fEHd�$#uU��$����}����'S��[�Q�m8�,&�<]�y����~�*����v_���pt�8ǉ�4� ��|nr�AYl����,�/^�d��B���9��!Q��	�/�b� �ik��=���x��I%\����_��'A��[���T�w�[�6щ�p#[f���U	2(eZyY0+6[�>�-���L�)@���M��l�>����l3텅�sSD3ɒ�:x�q�M�� cp��b�9�ɓ$����~|������4\u�d�뵷��#�C\�Ul���<NԡC�'��9y<� �Z�4r�-Cas��{Q��V2eK��iu�����y��f�|7I_�C�4	6FP����TC�H�����vb��-�E$�xi}G�<'7d�qP���T�L���)��b�(-ta,h2�樜�b��Lü׫V�>��ӗ����Iqs��ϯ��7���c9���KQ�dN�59r�Q �b(��I!g��_��G�tu�$$����d�Y��r����� HM�������D'���-����Ȗ0�y65+��_���U �q��y�{(�A��(�H2Օ���|w��0����Rs��>,>�zʌL|R
���Bz��=FP�׫��v�2��<Q�6��y�#�.O�I��H'�Ӱw=�����n|���ϝb>�ܾ�nn�#��A���a�idɆ^�8ψ��ߍ�]���a�N����y�d	q%�J��q$�/�2���E|t�'p^��΅��6�%em�c"GEg2����X�@��"0^�f��2����[=o�k�
>�Ϊ��d�TǙ�����9-����;�BG� M��g>�pd �����>-|�-]��pE-�)֪�l~b�b�(a�9=�?��o��W�t��z���O��]��0��m3�G8=ҷ��G���.�V�65�h�2�K/9��,�zSD��������˓x�����Q��WO����ؚ2��ʏ
�D�	k�����R6!�Wvc�t�H�I��}���l\�Z������8
�a/[�p0qʪ�-�a�؛&��{���vso�y�S�m����k����(�}\�G7f��/I��G9^>��3	_�z�I���',ӳp��z�r�q�
��+�����~rHV�ATy����sa��x����&=��>��9���D�^��c!��s����Y�2�>z��H?p	�4t:d7d�7yH�y9�9b�H_��Y��tZ��Ԏ�
ސw��(��)9�p�ӏ:%\i���$� ����$ޖ����<���	�o�?�������/��_t��G�>�ǁ#f7t8y��6�����يC(�Og�ؾ%��yI��M}NT�����A.����kP	o���1�᜛�'��WC�_�?$^%2�a�9_/�(��Iu��f�wmƙ�7v^��ԁ�17}f�G���NE�<�uL;"�-���ξ̖�z������b�������wrv<�Hf�#p�ck��Q���8��rҗ]i�rܑ>���9;���z�wI]�8�#jw:�<��V�Ъ��MDJ��D	
�{Jl qlk6��硊WîF��_�0��e�uP6E�\���f��s�Y�)&������q�ʼ�5�a��i��O.^�'�{���_�qp�w��nG��S#3j���?s0 �,#K��!�RKP�58�X�َ\�����`g�4��)�������=Mf����K��7�z^�(70T|���I8j��p*�s^&1Φ�ltxs�7Q�i��y���v����A����kQ�T���B�[�,]�� t�� ����`���{��?����0M���.nN]w�~�߹C��՞	���j�A�Z�['[���\���!("����|Z0�7������2��eZ�.-�������NnS��K��N��e"1��Ai"���>�Ƥ|��5��A�N�s��5�ɩ7���6W�>��ĭ�玷�\Ė��������1�5	���}�ke�1G�vI��9�i®�47S+U�T��{�Ã��bK��-ks���=x� c���I�dc�[�k,�i��ᦣ�*l�-Zl�Nro��w�|<N���>ɽ��_ID�<�^���I1v��&m��Ѫ���]ƩQ]��������3X�J��z!ϯDJw@&Q���,�(�\���km�U��cT/��5���$by�d�sR-s�Gt��X' �G���IW��^�dT>����R���DXK�@�T�zn��i���IkX�>��Ǖ"���K�Ow<�	ާ���L�W��)�=���$��������L&��Ѭ+��6H�y�7�VE�<���cۏ����HKW[�\ͩ�N���ޔ�г�����'��{^ǉ�co[��>X�'w's �N�䧪M�+06����l�vR��y�e�<��jR���E���Ģ�*b�(,�怕%B1`��7���6KP�_���;@�?�Z�(���/���ӫkT{s��d�1;�%]^\�E��_,5����s�^"�pTi�ᑖ��K㶢k>{��$2Z#��t�
�q���ZH\�m��HT?��h�u�I�~-�#����!�y�$)�f�$�L�+.V)RO�e yݻ1ZNҤ�g�`0�bގ	<v�d�H[ ����¹\ [�5����.�$>#��m�2�آP�P�R�bj�B���� �.XK��^�1�C�H1eGm2x��1�5��	e�{$����    C����]!M�������-�#0|��p�Geo��?��q�e�hu�%)��@:�v1�����Vi`=,��,S�L32�c3���j=�Z�8�3��P\^AW����zc��ص�7e���əIE���!Dh!1Wa�;��T^�5Zw�ޙ8H#�y��������g�#��c�~��/0N1㸆E��q�0�3�յ"N��Pm�oe�-��6��`�e�����耒�G8s({g
�Q�������n.e�ϒ5�t�b�\�\�(���Y!*�6���0@� �,e�f	Ow"#��o���zau��|YP�P�IM�("`a���%< �;�)����ǋ0�əy�����zw��I���MZ���	��+��e�*�Ɨ��GCu=�i{g�ѵZwO�/F�lW�v-��$�!���r����up���P՚R4���F%��E�9��z.��"{@RG�YFV�w2x��}���>�<�9��*Y`(R�R��c[������t	g5�6�svh����H�S���������/����1�7{��X�A�3�x�T��?��Rm[�H;�z�)#T٨$�(b@��k�a/�o�����)%W7� 1��;��J^���+~�
*CA%T ���-��8�i�M'��
��8�v��`�]8K������C�iw]_^�Ohf�����G���������ݍ�\�q�p�a�Lw��4�`犮0��j��& {�S}<u]h��v#J���a���Q�m� �;ڵ����Y�љy���]�8 )�x�5���vt6�ƿ��Za�����dM��.&��"(?A%T�J�uX�&AQ�� =M�B+E�B�C�[��U��C��I�(��$��������}F�]����a}XW�^	-m�
�ڞ2�+����T��`�ow��(�oԝ��s�%��{��!)Q3�ťD��:@[�N{]�]׆6�0�4��������{�/B����P�2:&(E��U+���<�-�ˆ��C�+e���h�{�B �rن2��.��	Q4�X4i:�
(��I����>M9��,$��%I�g�5A���~�5�7?l�Nz�Fc&�8�ݦ�<��[3=�@�P�G�J�:|���8�\}Fv�ݍ�n�ވwӠ�7��`ȕ�P����:Ƙ�<�-m�֒p�F�"�P6��x��Qi?��D�?�d��@G�8ς\/�k6��#�e<!f�Jw��߫.�:��ȜP�r1m��#�o6���&a��{�餭�>a����&��d
��ڈw@�K���+��25�s�[���<I��?�ְ�~�G҇���O#�q$����ܳ����2������ש2�!_�,��f� C�n�f 7��뺳��c[9�}�M��'�ր����Մ��"���e;R`Js���p؏gqSFkD����S �h&x�LI������̛��X������_+I�����I\�z�w2wF��}���M9yfM����-Y��1���%��%��áB|���n�4d��Nr�"�i@�)�[�x]9�\^��R���v�!�7D��۾ˈ�m�	WD�ep��d����\<H�
<1SE�xtk�����a_�^,뻽.���!��G�2�ۉ���b�d��R��ZqC��37��P7%����L��X]Tk���6��thj����ֲG�B��7DL9z?��8l������3�w\}���6X��E�!�d]����n��#0����r���p;���ӳ�5xPZ_�w!�����{RN�o2PWd� �K4X <3q!6�3��n�i�Fr����fje�ʬ;e)j�y
������ܰS��F�!sy�y�"��gZ\��6����h���9,��*G�$��0�.ќ�1:���;]�{���U�VmƠ��g�@Y5���A����K�����#���77leU�Vk��ZYy�9�kNV6��Y�s)v���gɐr1�<1-Ď$�<�Ή�V�vK��?!�̶�T�_��}I��d�<'Gٔ��V�ڼ�;����7X��NN�H�{Ϝ������������p:�����;����>	QrOZ./�l?�b��0�wF���y�R]T�i��������qA��y.�b��۴ ?�G�F�B~��.D`c"AKdF|
NL�s�^qn�k����"	H��fl�I35�m�bĺ�{�MZ��")���ɏ�/�#6N="�qތ\��S+���x�%9�~�&�_���_k_Gl�<镇!���Nd�U��c�<�|�e��g"����:����mr�R���5i�m�;���f�0�扟�0�:���/e�C��'�x
��\��j�xJ�g��Q�B���%{���B�HY�)� �����*	�}�0�B?%xk����2Ŕ�C��_i�m��A���n+R�}�S1�#��a��<��[�7ņ��;H�=9���D�
�&�"mxs�c\kZag��9��\8�Er��n�P}o�uo���ޠ�J�G �.^_�_��<�1���C��NBj�R^�6L�}P��n"���Y�Ք��Z��u\nC��m�I�n�3�L��Qy�M�'�"�����Myo�vz~ �P�ns�emh�u�-f ��Up/�&������bu�.��U9��\��Ǔ��Py�34z�S��w����\v}�%��e��嚛�q;�3�&���#��c�/`�����hZ��a?؆�: �T����gcd���J x3D�p�lC��y������6�mc@Dz�^�aT=R�ە��s�l�-��ȒD(1�|�㌙��Fg��
 ]��Bj�Ɔ*0���PE	U�g[EX�H���vZ[��p<h�/��ρ���[��)K���lC$�̄�Ϝ�a����Q��nt+�~��?��L�<\����	�*~1�x��|#�ր�
�����5��+@��/�M*�lb�fnmR��M���Ѡ�5�>�
b��n��6�G ;��FE@�Hoؾ1�@,M�2URf���d�E6��]���cX��w�˯5�2��&�t��ݙh������)Um�4��̄Bٔr��{㮳=��~9�D���L�'�mXuk�bR�xT�Q��*6��M��*�C�n�*m�t���xt6���&���D�۰��v�����iR��2`ƞ7<�G�hw���-���DK���i� �k��@�N�m�v)"X`3,]��	~��&�7�r�+g]��ש�M;���?����G���X����rĥV�¥��9 j58{�	�6����K����S�l#n��t$���p�yʬ�i���r�M����0�o�H�(�~�M��`��\�Y��}��V��x~MOcx�%��7��GY��O�BYz��]�ry�IJ�r&�nF0�N4T=�v+��VԦ�Js�+Sr8��x����f��ɥ�����<�D�X�L:d�(��B���s�4�:�T��3?"]Ͱo%z�2_�1�Y��=׉O�S��e�F�-Yyǒrl��OӇ��v��&	�L��u�l�%'���Ѡ��>CƦ�ڼO���<��`�;�Bư���q�^X�Ձ?�,I�7�ax�������7�& ���a!��r��[��5����e(4��\�;�(�tX��)�Ѩʀnǣajx���i�,�1s.ֶ�S+C��S��k�M��"��h|�JK�S���\_s;��D'`0F:_�϶$E� e��C�����4�����d���������r����i w����U%LS��,�ܱ��cl,^�RkH��TwHH���8Z���7u=� ���7��s��f�YL�@Q�1r�%�/�r�Qjđ�B�v5�R�Sӥ��Of9��(K"˾'�]��f&�W��d�����8K���$>�\���������7�2['���@�<"vz{H��pèX�=c�=-�Cw��b��{hNȻ��6�#�ϥm��M?��-w�b�c��J>(�`�� ���l~�sPhn��}�1��T;I�Q�w�H����R=R�mR�J�϶����u�*M<�@r�����Y��2    �� ;�qN0ȇ	p��][dNd��z�͔8�ʇZ�h����%C0�����0A��ʝk� C&
Vp��6���>�!-��@m� ��S��E�C��fWUؗ�Wv�M�����I<�	?S�q�sȨ#p���5Ԋ �G`��TLj�	��T��2��c-jcL�W.����q/��hN�5YW�Wd�("�K'\�P���������R7��C��P�ʐ�c&�u��=d{tڦ�3�����i2��ڊ�X�o��^p�?.S�Y{ϊ*��Mgt�� �ܛT�����y���m�׹ƒ���;�� ������pd�3��!`�������0�~w�ʜɰ��Q0WG���h���G[��-��S[��H|�!��z����$C8�z�&�xꍻ����!Mɏ�b�?��K�/d^�e��/٧:M%�u)à֡�C0���ЄDK�$4�h�~a+z�i�a�r\cʼ����0��̍�Bl1 ����q\jF�U�O5��/�0������mLL�Y^+S�|&�)��Pbv\*��1Y�7&D����y2u�⠓qo0no�G�󰭢��L�A+�|��� �߾eM���w.4��I��`C�=I�>�*%ƨ���A��WJ*��H��6�4'�7i���#�z��:g��{g��)��!�S���^���1����0����'���7Kh@�ji�;���@i��8�,93ω�7lʆW,qf6��o8N�Y*k4��6���ӇHwG�n�x�z;s^�v��mE�]��je��A��ae�~��������0z�Na�N��N��XЗ�7ELd����)+ˇ�6zg&�EhȆ`�Oܗ�^ �3�aY�I
��d�j��h�\�nK��$e��],W���'u�p��1�H�;�=�ig1�vD)UR���-Թ6r']q��;�x��kG���Ib#ХO.�Q� R��h�8T�T�VV��Y����+�H+��Xt�Zc�Z4�E^Y&���צ54��c"j}Zm|"w�e���cj�!���F�O�sN ����3�%9E�(��i���N{���].�8��yC��X#��A߶38h�P2R���\oqÓ��g�ef�JZ)+�h�{d�nw���-oK��
�e;vN1�j�}�v��?�ʱ���C�\\^^9z|���z�0��mV�C^�E
��24j\�Z���+2ױ���N��$9�H�2��C����PaO��Q$dO
{R��&Q;A$���t؞X�G *�@.��Gx9_�Yf���H�#���|crY{�Y|�snk��B��{��ށ��=|�W�C��!��X!�Ɗ��>Qi �+ظ������2���)2���L:��C0�\������	�����I��Y:�j�M~���Nǣv]�_���Jsd ��ἶ�|i�O�=�2�S.���V��Qm��SO퀈b�f_���/�J����n[�`�q�,���ru�.��q^��),�N��]�ި.Ix���Y�*�	1�x�n��Q��3z:v4��p�߄�6�M<�ޯ�]vֲfl�
�G��r?c0��ph�U3��C�yL���5���OZ�d�I���8��y�s�R�.� ���u�T�iOobE7YљݟN���CyfH����m�(?����ڜ�O��mǴ{��\N\�,Xj�Kw|R���q竭�Jp���&���i�~�e��bW���@/�~����lSǎi���tK��@7�eC���T��m��x���^g4b:���;��'�e��2\j���1`N0RHK����G�a����bv�O�I�|�F�?G�������d<ݱ��H+��ń��Vf�JV�6X=�B��T�OrLO{�.x�T�(��c��=s��ǅZB�̂4o�����\�n�)1��d+y�.�2��*��rU	��˭Sl؅�Id��p��f�#P��m�W��nR��+N�vB�_�aQ��i.�t n\��w�\�T�%��I����VI��H{r��41��RX��"ƀC�E����'�8��f�\A�  :�pA�/�fl��j�T�ѨK�m�Ou��������`�Q[G,�OH�<�������C!��S!�J�v�����5���qw�hO�Gkɣ}�t{�/譫��!G��O&iYK�ڔ|{�#1)��B���v<��I٪��-[Sb	�<;7����FLS,P7`Kd^��'����w������U+&�U�Gm+���CK �P����x|�܉�7������#b�h���:x_�M������ ��r1{u�J���0��t֊ӈ߳���@����d�k�Ԥl�0z(U�����e$[$r΁��l�b�� �M�l�6��tt�3|�6��\'���Һ�N+:�y�VLG��R?ߨH,��`����N\�^��3�v�|y���,
K86�!;q�R�Q������V9���L�rl��� E�oUKP�y�p)�M�E�O��R�i1bG
���;3�	�wǙ:FP7x�}��g�ic{	�IhgF�kE�Oz�,�Q�؅(G�`ȑG�'���ڙ���알u�&3�[C���Qr��S����*��A��&�2�y�&X�E�"���7��qҸ�H*�8�dM��Qf5��hьQ��+G"~*�(��L=����E8���e��#{ Bң�1�fd�DI������E+|��{.��Cs�#��P��%R�ScX�f;U��eC�s�8Ҭh�pܦ�n����@�#��fEH߆�_�6^r�]�$���E8������j?)�7 ؜�*K��׉%����P�FV��Q���Ȁ<�(��s�k⒲�.�e���W�a�ً:���qty���M������U,��'���R5<$��,+R�DB��B����z�8ϸ.+�)q�=(�U�e�&؅2~�O��z<���y�@�&�;U��Dp�ݙa�R"%h�ϱD3�'�fo�]��a���zcot���ZtqI΃�ٳ$M�;>�7<a�[��ٳ����J�����|L��P+�P�U8�Ƙ�a��� �f^d��
3����G3F������dG�>v@(���Y�=^!�=!J2t(�d�_�S M#���Ҋ����1LI|^$�(l�����K�("ϑ��nwUH�7/�!�������=<a6Y,�҂_$>���A4��^wqD����`�����tkfa��p&���י�'ȰHi#v�+�N݂rj��9?�w��ް���>�9��j��j��t�ܢ��2M�L�5oh��f���н�# �F��b/�kN1B����Q�ՑJ}L�׆W�x�d����7����6�礈6��7n����։��1���S5P5�هܮ�f��^�	�{���v������z�ق��ӧ��L,�Ɠ�<Ba!l�sܝ%F����G�ffjM!�i���ȣ��Csiw��!�S5���&��F	��4���Î4�vL���'`:�g�L�H�{\��U,L��Mb<����e���s-�?|��.��5�#���k������/!�O܀f��=mF۝�:�wn�R�|���� �1�5]�O��Q�?S�RL��eh�R�`o��r�ܩ;������p����,���,��J�_����$FI�G�>�h����U��:���aZ�0����DV89�����E�y�UkCU*� Xc���3��Q���i�״����셻����s��B��m��ӌ���瀍B\]�<���A�7IA�?_��r����6dԎ�ѥ�<��
M���Q� Nc�q���A�Yo�?;+X~��`r-�H.����z+>�K�3I�����'h�:b�y�4	ރ�~��^�f�b����L�r"��U��2Dm��.`b�כ���Q�t�@������!q��{
�\F=p�#��O�Rh.�X�	��PLd[�OF_ и��IW���\~�x��w+X�����\<4��]|t�u�aezN��&{��G��#>��Hȧ�>�9�>�[    ��ǎ؊%�Y�#�@=آ<����FQ�p2�u]�^����G�9GJ~X��P1��(�飼����c��vK28�["��!���t����^�Z&�{Z��F�B[>TF(��%h�A<�6땻o�:R�;0k��g�فbRj|�죮�{��>����#����M.�ׄ$o�R��6"�)�;F��X�5��>t��c!(��knktn���+z�גZ`L��=w:�)̭��<��I�W1|�E��������1r�O�?f&�8G�&�S���X:�l¶�%���L]p�ЗCe���lG?6�o8R���vO��!����%v�NN����� � ���-)#�jS�ޒ/A���{�/0V��v(k���PM�,�ۄkN�φS��6�볚�����yGg�'�o[nA�x�-#���A|Ȝ����������voS��#iEۡ���W��/R�s�V� -Z�'_�!҇_p,:��"�vʫt'~Np�1�����MHB���%Z�c��bOm1��<���
�;3�jX,��}�����R�ƼIٜ�ߌwV�d�M���b��E ?8��L��+���e:�D�������Z�Wmݐ ��1���$[K��f��le2E�`#՜a3��5֛�7�`c���í̦3�g��e1S�Ⱕ��L����m�L(ń�`�BS�$����7�5�t���0F�������?_�>3L3�Q��\ݼ�aY�w�kX�['��4����T� �tVs�gs,2_����S������;@�ʜ����������ַa9���>^2��́���_a^/vs�s���Y
��G�5ܠ�^���[w�G`	GV�vO/9��N�tu�#���J��%[� 5D2J���k�aG4����S>Sd1D�^��Q5y�5sP|7�Y���eew�'(� rHkWmR-�,F3
/����H�;>Y�������Ǆ�$�w�*�y>lJ����X|��V�^˯sm6F����!ݔo��;oٓ��`�fq�\(d��0�W��0S���]~��-�%���`��S�.�L��Cs�r�2�es|�I���&oz]1�~�?��~�p��̭{d+��`,<�N��X���c�r���3��b��p�C^y~����l�oW�yt�Ӫ9�f6��A;o�r������#Ў7w!�鵣���h{f���+t� ��&C����7��CL쉶&�!
T0��鏒-��;���f�e� �"�Sll�ABw�u���>���\��8)b�u!����gt,�m�>��]��j����imc,�1�'͹0���RaH�y|��J�G��3n o�L�7�S؜�eoM�v#�F����#�}�I�]kS�k 80@N���=���'���W9����3��f��BZ��@�I�r��7�I�w�A�?n��7e
k33]�nJɦ��T���~�����R7��J�F�,�EU�
��U��3�3�m?1|� ����.�i�@P��Kϋ%�^C	�NnO��:�i�hP��f3&�B���P���d BC���D2:��s��#Ps��E�q ]6���F��@���ӌ6���'<�������'��'X�R@�>�,���P�z\�gA~�bH�$��#���:�E�;�.���?= /�Oi��O5Eh�m8w^���.�G`{�ͼG]^�A���e�.�I�� �� ����sd
�6(�9WFz�y����N�%㈙�1��Kۜ�`�K��l�l�'��.c�UN�0��U�`\7PHi�Pi^(��W��B��P�s�ig�y��pD�8�^�0�q������*@�K�R���\�Z�����%K/d;� kPr�Sr�r�$\s]dBQ�`i��r��2��μ�^d����_VJZ�.MZ-�w��+�{�-]�V���	ޑG`{���"��#�ў��VE�TFf�!r�:�\QH$�������|�^
S�ƭʲ��":����$ʋq�JIw���򿎑����%v�ϙ���rg�T�L�"z���Մ:W�:f���;��`2>��GD�)��R ���b���kn�������U���lk�hT�r!�v�@1�g/���V�U���>d�D+��ˋ�����aSL��CP��W|ua`��Ȃ
3)��3l��<�u�y⺝�σ�t�?�x���+�H\�����6]������3�"O"�&�B��I�
�';��XX(|�Oq�a\d��k���iB��ݥ�'�-bT�*�NQ!��;\j(��2T�>P�
�
<_�R��T�,�����B��z����`�U�#pkk�*g�h锯a狰@�P,�.�t��U�n����xK�?bj�A-�e��+4�D�ɷ˰?���[Z���3�$G�]O�d;Cۤ!	Bx�d��Ï�b/�,̙}Z1�j@�2.Yd��A+Ŵb��J�,m�A���U�>�x�!Ӏ������_k����-Qh�S���Y�8O��޹	�Y��ܝ&�82��`�cߍT���˭���۬�������{���ol�Z]g�#h�B�����p�m�+W1~U.���͋U��[��˙L��3�>[ؙ�yz�^���#1�I����g9�Ѫ�<hUߘ�h@m�E	jbTAZh��<�����$@�p+2����"Dc�~1�w��g��:��c f̛��sE�l�ї�˲�	������Tj0j����-7v�c���y�W'O���H�Y�#?t
\�@�)��uwh�P 9�V�f�qr{�A� �JnŒ{��퀪 �̎8�}3ȩ�گIP���:��TnF�7#qj�L����{f荇R��4	
�F`�N�6A�X�=Vߐ�D��p~q�J�,c�7���wjǾH����ime����)0�@)6ٔH��J�
i���_�
J�O�#�P���ib5��WSH�PŌ�B��������`؟����#ph,P�Y��AFngW�-k�����2�Uj�ߜ�P"3���BZ6��]��R[Cw�O�`��1�Ȣ���W͑v�T�|h���=�����c7G���D���M�=�wو�*0a51�A!2�yTE�ü^�5?��D��a=0��q�4�p}1�U3�
h+���R� ya��f��v˖,�3�����o��[�������8�0(�?��c���91/[0�u�Q(��x��2��S�s���Q��\R������+G5�(oK=X��毈gkg7B��y; ���#�t���T���ir��']A1Op����4�!���Y+1Y��3K�҇�e��/��9�=��%�))�a��ƍa�F�[���T36:���(�x�׍�u�1|�2F�2i���fҟ��&���}G��0�蓬ߘQ0#B��������<��FHӂ^�@�J�LE�&�;��S�ިuڔ}�;w���o�q$&$�K�!��	���n|�}
���kg��?|i)�C�K�)�7�EF�?LBL���,A�%(Y(�L�]n�����%X�d����x2����t�ɾ�����������e�Kg���_���$h��_"G���廈���W�4ٞ���1�� gC�}0�?l�< �����X�Q��"À-#E�,018��O���~��<�y�7�Qr��2��y����Ѕs�2AMZY�O�Բ�X�u�k[�Έ�\��5VK�W�D\��L�M��r�����<�ß����@T?�8�}Ll��w|>Ǌ�����f�x���"Ǳ�G�Q���1�_���(�d������O%b��{�y���{1��! �yD)��4���rU_y��H�W��(MF�Y�m��r9F��ڍCz��N�{1;_t
�q�s���e.�/�!�s�w�YBR�A��u �XS�h!:/������O��(��'��'H�����y���}�Ub<���7 � 3��`b��;&��`:��������/;�-�PZ�YԎ[m
��P9�7' e�^�W.�hA�����*`!6�x    X���w�z��gyj�	����s�� ��|�,���Aj+���Oh�:�%/&�b���=􉄺�*�<����q|׊߮�ە}��9=��tb8v��L�1I��5�N&��#��r*�L�Z6�Q����e2�X�Q~?�ad	]�I)
b'�5��J�F$�!vc�-\�����<5����=��)֑,!Ks��:(��׿��	����Υ�qj���&��\ ��	�ZX��Y�i�غ9Sn8�Z�n$q���K��*�)u1�W�?)��8�=u �&��U!¯�/�����2Rv��&ׁ��+�c�`H��GP1��CF��a,�����!I���_.�i�+︰��^l�D2Z�;�%�Qf5�,����#w���{�#�7t#/��9�(�!�o�+L(��Q�%}�[@����������Ȏ^ঊ��7Xt�7K�3)�����7���40*�4�x.E�O��V��J$wQ'����aJ;dc B.x���(�¿�,Ǫ�Ԑ6s��i/=�V��uj�N9J�M�>˃xL:G���ĉ<��;�qd�c����G��\���1��4��TaZpr�\�B��0������m�(��Pq�;(�b�;Ca�}+�z����*�>[%�A��kzz4�ȭ��-#���\�������,Ga9�.G�� �W�����qo��Ẓ���$���#���t_�Q�$b�f]2�I]�d2�}�(��*�ZE��HR��Ͱ�A�S1V�ȒF�2Z�]�Vq�����G+V�1Z�[�T!���]0����n+S�EF�9O\�|���Tƶ�`0�IT���8�.fH�6���d����XF��Ja�~OI�^b�"ʈ~�y�W�zsO]�1`?qr��H�z'Ĭol/=֟&r��'���v]�������QC�I�q��{g����w;z���ěN۫���qX�+:#�M����(�If�(`%{��A;e��Wa{�n�\����*߯�Fd�D�U(���܀.��(�P-�%�~��[��u� bL'�&�:�ٱ�(���<y�s���BKnv�NƮ����Y�$���5y\����~D�=�w  .� �S�H.��Q����|��*��1|�w���O#��p�o��e\��6�F�ր��Ø��S�����f���,�j9F.�� �<-���4�B�Z�u�$����9Q()=�&���A3މ�;G�j4��0�V��K+3�5�����dm�R�XȰi�9��i���H�s��<$Ǥǝ�'�U.���V���3C�q�} 8s���L1}���'dKr^�~ҡɕ�J����+��
/:"�\��zq=����0ru��B�'���И��U�{�$t� Z5'�'��"�Wa�C���%�Cغ�[wʭ;1���L~h�5`��$wS}�Լl�������o"2XLrYMg�C�ˉ,l��	��l��-��0k��9r��W��@�u 
7!��;��S�b�sٳXv<���˲�9 �P��On�Nt�<Lc�M��:���� �#>׍<*��z>���{��ȱ�lp�z
X�J]e�@ܩ��ɼT^X��de�e�0d �D2Y�E�<�����0k�I?����9�0 )��Ԙ�ݙ���9����?�8s4��V������-���H�JS��^Ix�}�����ux���:�����q���w9���U���7�/�"Cٕ���f� {�Y�:@�w����xe��hMy�Y\��,�Q�ŁǾsx�M��»e�?S�{'V����{��4W@�j�=j��؄�eo�O¯��`\��=����5��pI�#l,pN-nJ-T �T�C;g%R~�}8�3t�F��B��>Êt�3Ǿ����N����7"�U��(�����+*�c�҇}�ΪBTd!�Y��p�n#��p��u(p�jѺKȷʩ�t���hB���y��)?���z[��N��c��"Z7��f^�P+�-���eL�U��ፎ/�.3������d����Z�r� z<���@k$�Y����<�{��H�ٜ_�@p���T�~�V§��f���Y�]�w:l�Y����Nu�H�Z!/���@�\<�������!�� ��/�o7���et/7���:�J��x�;ڿ�pjW!�2��D֥W�Md�"Z�� �̽]�YQR���ֿ�<�àјheq��;�6WS^�k��GB�l���7ⴆ9;��V�����)��.����2,Y.��v��a�}�k�6(ĕ �Gz���@�?T��k�k��&��}�go�b7@�>0�8��_ѬAdg�_ ������g�>1�E���|�Q�p�r�翟�U�n��Jb)���䈲�;8�R:U�p]����˳�WJͩ�Q+�T���s�-�YR��p�p7暦ކ�}(W�y��%q�W �2�s�|(����sa��=$䰹k[b��N����b��q��Ya��(4��}!ud��|O��R.�-�#�+M��f'\�K�})[k�^j�w�+E� ���j7�����D��(b����]���}F��(��1P��D=��s����5��r�0���b+d+��u��܆*l�K����`˼L3D6"�$/�x�f�܈'���tk�9yx��aQ�-��d	n�Rg�8��p��\0��{��\)FV�ۂ<^���kV/�h���%��7�o�m���h��~7�#lB�^>��<]cpr�	�Ð*��Jp�m�U�]�AE�� �t��|�,C�x��W�����\Z5�E���vi�|��6�v�\���O��7
�ro�Q!���Vk�s���⌨eY��*IS�c��_�Vofwm#��+!��y'�Ж�X��ߣ�'֢濭C��8��*�r�l�ax���ocW�RA��bIN�s�bHjKrS�]C\4w��8��z������ڤ�̟����Gؤ����8���շ�,�EYn�ħ���s٧���Yd�f�B���&�Y���M�Pq4��w�9��������"���	��"�N81���w��U�:�x���l�q�JdQd���"W���C�3YS����,����V*#:Y���`E,��y�3~����)�t`������!���БK{Vc��CkȆ\\�xZԱ!��Bj�5�0W��#��l�����xh]F-oE��cO���\o��胣/ˤ�y�Go=�̉����*~7��k���Ag�|R/h��I���0�5\�g�u����D���1ʼŠ��٘�!�Ǖ���ȫ_�:Ny��D���h4YL~G9�j8_կ!$��V�K����N�O�4\{��+�����}g�:b��5ܮ�����2�WEre	a���[i�c'�!�&߲v&e����T��ƕZE{"��[����d�
�/�� ր1�V� � �G�w���j^��r[��)v��j�&�4��b��ťL�*RB�y�N���@��1��<�����d�����&��k�N��Ɂ����s��MH����^�F2j��$�s�!37������[�F9[�����	��FӍ��ń2��@<:��X�˄t��sM0���O�o9���y�:���/۵"�������#�HE��$�*"q��º���I���`!����{���]����\`j@���!!�m�����-��=��4�]XQ���T��8��m��ĭ�lڿU�5�_��Y�$�*y��lK��w���~�p=b;��؟�1�Yy�j82��&���e�u��d�=�����k�Y�Ͽ;�zP
�2~�4��r��p�������;Ѳ�\�7hȪ�h=��
[�a��'$,�4Oq�`������i���k�F�H�~��L�ʫ��S~�4��4 �o��J��4���<�^�+9r~��uj���6��<�ܫ�}�\�|�W�.|�w������HnjԼ1:6X)�{9/�Y���]�&&b�Uj���de�E��*:ff���ap�<J��Z�ٞ{�B����ҊV�D��)HC {����D���    p���O�9�U��`�����? tpU �+���Gkl9���7�/#ۏB�S��n���1�)��x��YƆ�ފ����jn�ٱ?��"�~�۵���y[w��60Q1ob��"����䠛��r���j�̽`0�����{�p�m6��85n�e#�@1zm���p2��h���q�����濧�N�I8<����W�ٱ��ḵ�G�H_P�ȩШ��l�;=���=[afQ�m�C'�z,�;q�,�g�����P�x��=��j�b�+�QeŹ����&TWn�5�f�v���j	�m�C��$��M�OgQ|F���飈��b�߳��yV\�fW�n��29���q����wG��&�bZr�"�1B9]���R�c��������|���B񣣣?����J�����]��7��K���(�I��5��&�_��'7�~���4���
��w)�?��Ֆ����X좨�2�QrUD4�Ȏ>�*&���EtIf�=�վO�zX��t�'@�]�5n��T6*�¬�}Q���mB�}JQ5P#]YQ8a�HQ�V��ldF��3aF{LΊ�ח���;?���u��/�|o��u�N���=��� �Q��7�3��NW�+�S�
|m�jU���v�&������(�E����L�BT���a�d� ��l��@��g�>-����C0����m[�i%Q���jOn
=U7���/�/��r���~>�9��]����t8���l��+k�Ŏ߆�%��t��a�ؙ��Ve�z��*]�ksK�0��Ë"�o��W���U-sЁi���D�r���zgE^�M #/�h��egoП�Ώ\U�X>7`Z�]q��׌����rP.�у�b(VaL��+�Y�g�5�p���?��u��h��l���p�6*+W��<�>E"0S�]�v��e����������/�Ϸ�u�A9��;���&�/GwUQʧ�E9:�2N��U�������.�����(%@D>������w�:�x�C=Y������#����f�d�J2�Bs�ڀ�M�U ��h�4"B��h�*c�7�*����_���&��'���n���a�c����VH�#(��p��״CX\�3���1��)�ɶї=s�脶[(��l�7�p�n�k�y٦�/>��,_re��l���H�:�y�HK�E �����_U��� ���2�<īN&���Ӕj�Pg5Ck��`�IHª�ds���_ޮ�?��z����Çٟ{�9�Sv���d���/Omm+�]d�E"�������i��m%��dhY )v�3�o��$8N���h<�^�]j,	��������G��75��p0��fÇ*@����CN�����^��8s�浨ߐ���3f �����VW��Foֳ�߼�W��ן>햣���[qt���G�ռ�d�l�����Hie�q���;ۦq�����p�#g����)M-gW��1�~}�D5�$��Zi�K!�,?��#ͪ�^�^����eT+�.; ��m�~&ڴ$ݑ��Vk��{�{Ǝ�l +�f����7��L'��B����Oo>���~���_\$�n���Y�2E_îGP���g�%)�2����3�k0?�*��9�s\7�$�*�D�qk$hG��%�����iy�}�̖[l@�x1olf"����#��U�.>P;5K�M�
�����#�p2WJ�h��iC��sWWK��~	��Oyf�g?�gwo�=�^��l��s�_1���p��(p��� ��E��9O��a�� 1�ed+'Á���d?%JG]�V�"h�h�ѧ�F����Q��hM_�9�8;1� "�I}>�-����q\>ѐ���N�{�^�T��8A��vJ*�X�x�}q?����{����x���닏i�/��?l#�)A���c�xZ��"��n��2�$�s���_�R����?�"7b�#=�4��[�_�`$�"�RVB3?�6�.�P�˕n�`8�1�`�O5�^�v-�r�xZ�6jXY*�v^�hY"s�����|f�<2���@ϒ�nڱ���d��g�DZ�C�o,�;H�9��[�qO<s�<Ϩ^�/�BF�^L�C��u�����(��>_��>������x:>�����\�{��ˈ����e���x�[)�f_^QV������?6���(��.�s�M��p�V4ީ�ʽ����o�U�y��tἎ�"݆�s��y[�py�6�Ɠ}IЬ]
嫟�7a�!�<����6xS�+,�i��R�J����u�����a�6���'�j7�/��Cڼ��J�Xi;%�H�ʱ�!D�ʗ!e�8�Ï���~���>�gI��w/��.��l<k���#,��|q*��2�C�<�����7�r���?��l�m�k{s���'?N������?GZ�t�,܀gG�c&�P01׮�$��w�D!���@�����i���[����g������c���jExpQUb�13�4���࿖�u�5���xZ6ZI�� :������Ǳ����Yj�������ْ�]yl�3��KCr���~�bL
������LX�y�ee���;�q0�8�|Z>:'I�'��3_)�nbqP�[���,,��泸m|=�f��v}Z�{�T��>h� �6� }��[.���A���5܆�bླྀه��� �_�aR|a��*a�:�Vm�/y��4Q�T�H�J~�;W�s&�VS_Cj=b��^m«��[n�,4v�k2���Ě_�?�]��9G��o�(�}�¾pz&�1��N��J�)�UI���>���ʭ�Ai&*�k<�e#E�ʚ���[/��Ft��
�����JC�k{����ZO
^o�Cǃ����K�����h[x��T7|��jWƬѠ�uI@��*��/(����Qy����\��J}�1(�~�⿳t�����}#�}Ud[���5|h�������9����,�5�?��Jςg�on�m������;�����s�2�9�1����w}Cl9�-�Vb����
q���J��ҭ�!�L�g�Jǔ�6*���&}�k1�N���1}�۩[����Ȯ]k'K�"����ܷ��ﴨ�:4���Y��z��r`w�p��a�H1��c�wM�To~g�状.����ڰ.�:����ӥ����t�HЫh��}��Qb� ��{�x�˷��%�_���2f[e��j���q���~�} ������1��W�aXW�q�i�E��L��SScL��1}���DE��Q���ؑ441���4���W�G�����ӊ�]�]���=���I͌l���|n���XԌ�� �tE�ۧ,ڂXTSR0����cS�!������pj�����ȟ}�1=_��&AGKn������q֠��UFFe�)&�G�W"��kSaAЧ��p~�I�GP����*�Ａ�F��#W~F���b����Jy�0�\L�F�(�C�ڛ	3L��b��j&�����)�Ɗ���hū�l3Ҋ�z��X����Ls���ǝ�Iғ���y��򨃀�J�c��w������̨Ĵ��H�@R����ʙ�j��ET��h2��ix2�ϺAÌ��tI�����('#�`�7��X�!u)έ�V���l��@��X�|��Z˜�x%�%�����d���s晪ɂ�F�<e�	���U���ʐә�����tbW��j� ��zw����W�/@��e(.��1uq���g1�UW�����FD�	��XJ���)����)��.�ufOyk^�kspO�;�A=5�14Z�P�u�R��J�@p0�y�.ż?&�R��$���q̛T�x�{ϧ�道��������Z!V�,eQ/4K����*y�w��e��(P���f`kS4�~�E�����:I,Ba��?{=`VmP�/ߦ�I~�[��1�ǟpY�!�­j����h޽����0�]���8~Z�sɥÊQp�2_Q�s}��R1�L��Q!p��Û�5��"[��;��'��͘ư9XMSo���ۉ��žd��    8�U(��-�n ����Nm�"艧c�.�Or0Mq�ʠ�0�F�E0�������x�(�{����s�/�8ox�;Xk$)�Z}2b�@�;���'�鮸Lh��E@w��hIl��c���J/�8��_�{UL����#�n��M-{��t^�HwV�����i�(�x?��}t��Щ�ۈ����+��r�ݻ]�F��&�Qo�s����
$����ϻ�o�~AY��d(�.�s]�/J�@,=h�[�{��K3��`�N�G8X���^��T<�]jS��/(ҟwm�N��ۥ�s���iY>Le�䆓0��0�M3*������k�he�.A��d\�^�d)���*M���W�G���
�@�[
�b��i�>�h��쾰!�a�-s���ͅ���(cq+����p2<rcP+�Y�٥| ���%�OK\�2A�$0�>���L�Ўt(�>+azt��ʗ1�����{3'9=Ү9jhM%g��I�Pr9�5t���DqS��bY>�N�%(�Oy	�-	���=��K|�6a*��)m� �&��V���F����2�5%ќE�R�{g��%)0����M�0|��rku��� �<���:7��)`7�e*!�IU�]
�8յ6�{/��d<:�5>���s�F�hX��B�5��|e�X��W��"�����
�_�a-9aٿ����]���/ e�3��]�::E��G�lv�~*�2Eg1�MS~�0��t�v�Dm���?�Q>�F�UDI,:�5j}�AU8��2�X�q�Bݽ�-�-�H�Q�٠k3m���KvQS����0"R)z����6�f�:))33���h|]_�@UM͌�4�Cp��w���n���2x%���ᥕ��Ŏ�̀���:�>�:��wh�ђ��P�c�������)�=|��S�te��Ԙ�g�}W�F2�M��D��Aa.S6-xe���B}�eF���K���y��Ť�ܼ|0�p��xE~��zUk��n�::Sk�c���R.[�j�;:���@�N��Z��Ff �G~�+�cD�����h䏻q"��p4�ziy�L�51����ϱ[VcBȑ����bu-�9��Q�\�[�S�v�f=�
����L^�p��+��ʒ�9�cGD�:��L�G����`�|�ė�f�/�"!$Eb�H�N���r�"5��_�]����(�ZR�����o�X��u�p�="�����݁Uf�ҽ�,VY! |z_N�h�d�� m�V����Q��R�;L��d)�lS��>�ğ����{��4M�A�N�j��א�ڌr����J�M�?FKdn9����jU�9��}�ݰ"o*T�w@�*����B��������j���r��\��h0G��� 1S���`�	�
�JG����*��&�>L���k�A�e���įb�o�s��/��pz���,fÖ4uu΢�7�&������趍�ld=����@P�s�u���?�Q�;�����o���>�G8�uc�.�J�l(#�6��=%`8r�*]%�o&bv���d�W<��Ꚛ��^M�H�~M-s�����m-�t}Cni��D�U^�kv�=���!�ݍ��_��UG�W+ܭ̘&���M����Y*�� V�kh��<�Da����3��q�U��������%��U��w;}s�f���=��x܆�q�DES��o���h�Vz�pG�ކI�.o��_o���e�[NY`���Ff��Z$�S���ɸ���ק�g$�!��n;��2Uu��i_�/�w�.8}��-��w�������Lr#�B ��N�>�4@�N�X_��_Z���
�NvYL���)T:�_�Y���9~� Cy�n$�<F��֎�\���7����A �i�L�dX�����1pmJQb1�	�0�G��7��Vٽ��q�˷���~�a�g-[��?���zca��	5�	9�J�Mu���I��-�?}���sL�\!=Z�I��rJFfY*ged8������Q0ڝR}-�Ԟ��o�C-v$^׍���ʵ�̽���W���.�
��n���j˧��*��A�-�<d�P����q�����p�-k�����_�y�ߣ�?,cߡ��Ӂ>��Y"~����Ph��2NN&϶i���`]0�����G��ח�~��*��^�X�ʽ(���qd�G`�C��`4�zh?���\D̘!�V�Q>��`�O^U�E�x	�[^j/�-FolO�� T7F"8Ec+Ee��Aj�M��T ��gc�FI.�c@Wv�fs����,,r)7�����ޚ�����b����#�� N��aK�_��,~�{a��\��K�q��-�"�v�C����%(�K��iU��6*�����E��ee���x�(�)��"��|���s6w�r#3�Y�_wf��Ȍ��qI�m8ccgat5-z�����0h�պGP]�w0qz~�^[�Mx�ɻJ��=Dt�΢{��XǗE���F}^o���>�7	=?z~T� 6�*�H���y��_v�3"ߊ�G���-?�2L�K�_�w�N�N>����8�:������TLtk8��I7솭����۽@}�T��^��\##8�Ηkvs>�G@�xg��^?�5���cc����(	W�(�����)�9���)��4�T�i�:�Ew��Y�ͪi��"H��
Z����9o�wT� ���}Ɔ��f6��#{�'1�A-Y����+y�����?��NMl�M�ɺ����QX�=�g��=G�9�x��{QϽ0��k�0���c�	D���`��F�hx�y�G����?!eL� T��  �]v�n���W���4���e��ũ��|��FV������U$��V�����_��>��5��������u��s��������w��Z����������G�B共�i�E��p+BC' ��K�nI��,�=��cDǳ�VGE��q��Y�}�b`wn-��e�`���貴��kw@�7�h�^��U�~��Y)%#J����� 5��;�v��ߔ��	+~��9D,k��'w�_t#R�h*�w R�@�6�ɏ�JP0�9)b�����S��{f(���|�w��f�p}���<c�c�Z1�9�>>��
���z�-�}u�+�Ƽ�>�q�I	�'K{����r�H?Q1�`�I��d �=},�\v6�O'/:t��*�W]X�;�ɛ�e��V��ӭ��1�7S��6�%w������{QD��lz ������ _�X��j��p�\�hycbRۇq�8BI�x0�'�d!ڢ�Q���I¤H���aw�e��s�YCe��~Bb�3#r�G�n�|��\��L�N���o��¸�Nq�T䧆�Lڎ8V$Ɖ���Y_�x4�JW�#��۞={8��P1��QK�U��� ͹��,S�j�(M�!�"�֙#��,|��|��i��`������}��qF�FQ��3�(k��J�y^�]zu"�5��mj�R j�	��.���.��9Ӯ2dx�Q�%5a�	ca�
QS��t�Lp�6&SIG�e##����b4�3�G3����X1�ݎUi���T�`8,˹�kf���p��޳���=�F��E~u���C��2A�ی�|�b�)�~ʑ:y�P�K��Y�S�S"ic�˘����|Uo?�'}���|:>��G/c?�������#0��DS�h��@�i�5�g�j큺�)1x]B���<
�^&�Ձ�|`�Oݮ��$қ����l<�f|�1P�2D�VD�H��u�����ӏ'�K�cH���+ȝ��'W���;��8�t�j�}�����d��-S��T�hM��¦�zQ�c_����� (羻N4�2�e�z3I3� S�iū}r�*�OiR �����%g���?�$�'�ōl��>|���R�Gxډt�$�Vv�����K�9�i�]?cZ���+m�@ν�1��6��r�_���-b,����������ޔ�����W���2��t7�3���-�����Қ�wD
޷����^ɎZ7����-���    ⑎gm��T}��X$�m�h�l�r�9&�  ����e�r˵�|�,l��vd�	�99Em ��=�e咵xfD������������K��'�|����R�!��$S�S@������)썎�p�Ǝ�!�>~��d4l�_��$�t���J7reG���K��U�A�d+-ZE��|öl�]�+oɴ�נ�5i	��9�x�����Y�"eU�C����I�h[�+g{�v>���'��7v
�2n��$��	�����E�����=����)�3���*��ׅ��^�&�D���?��/�W$	K�~���G�ú�%y��8���wfK8P�^X�\[^=�����y�I����;�i߻*7G����;3tn���h���bV�����Q�q�l*��1����M�=���3���+d=��`�!Y����b�p�T"��5����ßF �,+_��k�2�poO����A�,uuq��r�ɍҶ�;E�:W�ש�����y�P�j�h`m�����dTjh��Mw��ɰ����E�?�xW�/+���OF<���W&Xx9(���ԕc�����ve���[�r��)uV�ב+�*�׊�O�I�1ON�N�N�G���Q"]��8���i�$6�LɔS2�;E�"����Dl�AO�၄,�@�Z{^���[�+�9�X˅�;�I(����O�r�D�  �!��p{o�:C���D��Y����f�%��r\1���)蚯@΅����^r������1)���3N>dc�ƈE=�(i;�OCG�i_���v�AAK�P_kfG��RY7lѐ�E��L?�L'ϱ8�lݞ���aH�[f\�h���H��H��`�5ꄕ������UTN�p��MTk��D�`�5����l���9o���Ï E�GQb�8�Ǖ�h'z¿�q4�6�Z�Z{�-.�tP�kHV��M�� �j�e
D���v�ّ�0�5|�7Q4���JYe������ŘX+�:MWDQ���P<D�ݠ4�U:�9�s<zLlb�	ř��t�,8͆m�UZ�C�J���q�à":O&���Z}���}�eJ�~0+Æ%^XR� ��N��s�i�`�p[����)h�m6]�L�A�e7��dVvAʼ��t\o�0Z��h1��YC��Q;�ae�����N�J@��]W����:	A�F�voPA's��I��_���]�r��� v;�Ĩ�b�`��4�#W���lVJy'K&�kl�@�w�΢x^k�`�0��RXh�&.63�\&HO�'?�3��ڶE��$�;��K�;��I|e9�����%�'��41��E?'3������K��o�:��ٮ]|���~��J-��,X���5rQ�)pg��ua%��E���h����aK�^r�v��4la�嗠���/T2��ڳ��H�����������V��u���P��4�,I����ѽ�m5'�Ȗ^����5_�_a�6�sx��E�ğ�w�kto���	%}@��4��F>51ލZT�V+@ǋ��';k��6:k�f��а��d:-0��#h����v����#t�^�ܾ�T�3Cuuf<X�Bj��i���x�o�9/��%�y��]�Ӥ�U���$o���&7n(e�4�F��R��>ge�����G����z�	T�Q%��ŮSJDrS"�DZf���|l׬�w8�x��]�}�"t�e��d���N�.� c����!�ɓ�
�P!�Q+XX�A��Ne�P˴��f
P����Bg���I唸.�#��{�jSp��-���e̎�Zn�G�6d�JqȭZ�gO����=����`K�/�tGv�<��2t�.r�grvۗ,�mK��N�ډ6r�彲N4dJ����.e�h�J��K,P�1�Ξyl횲�0?v�~����ڣ���?;�+λ��m��B9�d2*�\Z�Yco�>A� p ?k�ca�4],���h1k%ev�����%R֚����\�<`���s0%vV��]�w>���[���X�9!ᢥ��zu�W��z�ܱ���Zö́���l��:�i�eެ���WY8�*	<L����;y�/C���H��n��$�	c�/�/�;���U�U��X�T���S{��}��B@�m�����+{.^iZ�q�1�'���`���dJ$�eAC��UVXK��A�euPʍ�}���N�g�~�	ٶ�PH ���3W:�����ß�%b��.�m��h��m:�[�|}���2�e$g˻�2C��N�<�"k�Y���N��%C�/�3��JR�m܎��{�:�V��)�ǌEc|�#�t��Al�_.���z"��b#.�����r�(	:K��FU"z��=�y��ғc�2O5�>�5��Cp�毬�s�`��)hA�:�\��H���h���X�E_�c:�M���#,��C��oł#�p�ܣ�%�a�&����l����!Ed܎��.u�i������v�"�F؋��T�*1�[x
E��:Zgə��Yrč���B�3h���������p����Z���8�x'wa���]�9�ޠ1��+��!��"?���s�����̺�C��Oa�Mz(�7��k�]*�Q��"��u�d��*X|gU �1\�Z���r�K�`!fpX#ƹfr�����Z�Z�~#�n���
�"�:��aw~N�g��X�N^ ��r%W�2�+ê�vGN��VՎ��՜���=¦�r-��J^y�}isⰈ��&���2rM����hRV��r��94�읔�����.疗�Kن	E����7Ռ(W/|�SP[j�P,<��v�D�&?���������]n�i�m�Q��ݤ���	a[љ��>�O���{z��e]�j�����_|ݐ��e�"��$�닖��>$yL¾0�lQ�!](�����y+��n�U�E������ۼ��9y��S��M��9Ī��J�!wk��}w�z:O�br34i���gá��^��mbE���j����������ISD�av�ΰ�Nl>�,�%`������b�fDw����?L�y�$�w�ܺ�Ë ��O[ �?	]$*]�&�����H$�U���}�	����t��8�<�S΢}7B��~<׽`�Q4�9����"��Q\v<<@m^2��9 T�K%%�!��Ć��֓�5Ƭ��u2��mJ������Q���)kZu�_k���n�q0�t=�j�|1��YƆ)��iA�Vsԗ�	�+`(����4��n�:R����"ˁ����0�ȿ�R��Q�����p�K�V��,Eь��K�)kcb�OO#w�d.h�x)0D�4&���u�p�����`a�>�v��m��^�W-�DtS�9�� q�mq�EG#����M&Y,I����'؇s���7o�16;���6fC�Ln�nt3j���	�eGGG�=�� �̶�Z���c���f�T]gm+(÷~�W��#%�M��<�>�� {a�b췢�॒��G\1��2p�J�M������l���D1��Y��F��c�3Nn��E�����r�Z1�׃�ދ����U�$�����/α�s/�����x���3i<︻��2��*ӟ�	,��b��_ޯ#Gh�1��)��m�z��#<'^���+�ͧ�_�޵��G�컑��#N��c�9�0_��%U4�}�O���$�Ť�s�}�(�ħg���e�)�J���d��#W[���N�v��7��)Hq�zS�����yjX���xU됎@�������0�b���^>}�y$�Zr{Y�h��;fHuĆ#n����w��e�f�GP�Go�]�C�6���r������u):�ij�.c�V�T���c�{��ع(�n˖��5@�'����Cl�ٕ���.(˒Ҥ�m�$���SLw�v�b.F�nA��)�倿�%Z�A���@��@'�S<T�E��N��P�%���oHdu�O'�Nɇ��|I�״u���O}�i��E:j���q`$Tiɳ�R���&�ؗ�h��ۯ}��	    ��#�(9�?��<c?\A8��P��{^�kU�·�s߹Z�&�@�?�V�o�!g���\���<�X~������H��+ޯ���v���	*2E6�C��&�0����y��>�N��٤=��D3��qٮ��pζVd�����Ǔ�Ѭ����H�k�~�\GV���d�]���w���G2l��:'X�1�s����������d��>��\��;��8�/x"Ӌ@�&LJ�J��M΁4�@U�s[�����,M���϶^<�@�و	�n�ر�1vc���F����QU�D�>��;��K8@�(���M��Kb�ހyXj!{�(F����ko�D!mK��ұY�H���П>�*���L9h�jr�k����T#6:� ǽ���8س|��*� �vƆ7�n�/"���EI�("kV�_\d	}����>���9J������X�����$�?����F�c7n�F�N�������R�����o� �A�t�HT�.�GK�E"���j��y|�ZL7o��V$�L62U��<���G�߁OR��\�D���
!�m0hC,Y9h�A7�׋�_��7: =>������㹍�Z]q|!�¢it�?�${��oHDP���a`#���[!�*6wr��`�z}�c5GToU�`���`qT�oJ�Uz���Rs��d�H���m����?.�q��m�"7:��2'���t�{Y;A0��[���������l����T�;�Dm�>zˣ'U��������}��z~��
��+��`��E�*D"}����*,��+ܚ%��#m�<�99���,�ߓ7��&}�Q�j.Y��p􆀵k���W�z:��K����ג�^k�ȡAB�*6�,�=h�"�Q�q�:-�u=���l��h,k��t�n�}2Ȱ�;4W�T.�M�����\���]����p%�"�Q��8��ș���xRj�٢��r�[y�l�2_��S�� 4n��pM
Iz]/_�W��_�����9���͉�Xǣ�@�o(,C�V\߹�cejr���7�SCwc�x�OF��]Aw�^bɢ���'�U�We���v��k�Tv�*	z�?�_��p��g�(�`]�K��C���X͋��^|x�]ُS���������u���%�#Fw��}{d�������N0׷!'����E��-&�,� J��3SH(���6ֳ,ڃ��g{mR�z}��;{~(tV���7��e�Vf��l�ڪ�S��K dq�aϨ�> ��e�w+�Zn�_�Z�Es�?i&�6�^Eiv��ҩ���k�]��d�96��!w�2w�Q�wq��%ƹOC�Aw=-������Th���FZ���<NHp3�C6
�A���.ɧ�Q`����L�#�1��('ܸ3�ɦ� ���x�K(�ӭ#U �f\,
��V��!X���O�)w��Np��9S�Y��r3��g�K0��]�g��녏�J���T�Ƶ�岬��M,�NM�r�����!캸,�{ԮYa&4�"ģ�RD�B̞V(NFySt��� ?�����8c�°�u�fb�rj;�{3��_�����UG��d<l�1�#h����'"bׄ�>;�x��E�Â��r��4��ѡ�h$-�C���*�T��<� �x��3-C�-���ˏ�<�k\���&�ߺ�1�k�'�J����ѹ���Ҹ��g�sbs���P����������=�2�DY�9q�u�j:hS���*\� ��J�%V��`�W\#��bu�i���؝���,3��ě�M6a}�u�9X6&OiD��$Crq�D����f ]�o����G�,&�)T���V��[���8��UqQ\�o��FAH���4�����,�5|�.��\K�a�W�I��p�+�Jׯp���x�(]�M��+�7�}�!��FOd�+y@-�i���>�k/*'�����+S�GCW�Đ(d����!AW��|�徇S��]��o\���u����C��b���U�ӣ�mM��5fu)l�=n�^ZG�8�>0ͷ�nw�#��x��N޻�N�TPq.QX�E���A�>wM�s�����l�Sk4/��ZN�u"'/Fb�:�5q�R�$�b٪������[�Ѥ���=�"�V�J���[��F���Z�� �ߋ��D4e
\�	V���˟�G�WtnFt�����Do��NԌ�9b�[������'��;"����'�����J���F��`�ڰ��4�\��<ڹ֩P�0�Ǟ�0!�����PO 	����b4DХv�n��I�p�A}��b(���#xq<~
.K����䤍s����&g89�zwUN���v����H7�E_�Z�?�ǳ���{��1�ђ�[�`�Gq@f
wc������"[@3�<�����Ա�(�[2T�"�Ϟ�
g�Oi����/� ���ءeX�����,�ݥ;�dr��!��
�_4Z�O��ײY,��t$�t��)m����G�c��Z�����4ԲJ%QhYL�("w�pt���Rz���K� Sɫ1vZ����l��BKW;C�jz]��ٴ���a;��{$b1�qz_A�?����.��&K��у��R"����$� �|'�qܤ�ovJm����y�}�W��;\��+��@'�Ѭ�U�����i}q1�p�bȲB��vf���ݰ��X�����pSd���>��K��n��ށ� �O�Qy�/�����q�n-�򝱓"��R�����&��l�+��d�
mv� \��� ����7ﺈWL�M�:�g�?���#�p�@\�3y�f��?�&�˼�>���ϨY��d^���ȔA����ɢ��MU�����{�cD���5��A���A��$X�ѹcǤ��� PИ㱘�ۛ�w!9�y
�����}&�l��
ؖb�:m����~�����ND��1�_%��7�~iPG�����ϬlȸG�r�?"����� ���m-Ud��*!�&��t��7�^l(&�b��|�=��辟2�p��/���|���N���B�B����h�u�^�<� ��8��˃��y�vR��������F�9!+e�^k{-꨽b[%L��|.���c)��s_(���ˠj���f/��Qa�MMX襔�[��O;j��Paz��Ө����A�2�ok��O�Jz���h�c�y�ޏ��6N�h�k��n�_�-���ؐ/A�,��u�a�RN��6����g�b�f���0����k���)(ڀ��mu��?a�m�pc0�^�&�����sȆ$t �F"���y�q��^J [���vQ5�����0��X��p*�b�SBL�\sحw����wҫ��p8m��4L��$_�7�7�w\�?��"J��vjq�,�=ߑ����s���y�c���I����6�ج�����PNح���1��'�t����2��k�p!&,�
�veh.�Þc6裴	�q8^��u%�����vBA	��;^]�<�p��+W�ʽ������0�Ș?;/)�2��i�� �*���*]n��R[4o�Z�LN4���X�|�ȡ�G}�>����<�K3������UkK�4�;o�5U;���,�@��7���2M��q�U�赵�
w�u݄6tV��X��')/&XBV1�!�52G��4�9���g�/����юGX?t�!;.�5~_4�PH.k !�κ��N喐5sg���;|.���У��;|�veO�U]��>�����U��*I���	���8���~��'62��/u�Fg� J.Z�M}��6:k|��sK6�O��h|�0�#�����)P,o�l�z·�{�&�Fs	�oA��}���Y?5WK&ް�In������.B��<����5�b#р�	�7��0�H��w]k���$=�)vF��%+�oK�}o/��CC�3��-��#��p�h-�w�D��a�������b��ש7c�W��n?�N<6
�s,������Vvm��d�;-�ťOB���ӣ�r��h��� ]��    ����h-#�;
Ī6��ѵ�v�G�dC+�ݬ���d�_v�%T�m��{�����R%gf��0*6#���fR3*5�5�<�,��Z���#(�W*���3Ǟx�W*�I�F^�t�F��2wR����z{���9��^��ѫ.\j[q�&�L�'�9<%�!�}��So�����r(���u"뇘5�J�+��D<]6=Cw����(_7�@��*?��4�� ��Q_�"Ŷ�sSI��Ȧ������G����Aߌ����&�g!j��Ud�,w���m�L֜��N�D/J)rg��q��-�dǝ���#�'��W����Kc`b���IuV�A�����N��ܥ�3���o�?�=���|���Kɸ�-����L�Qr��Сx��3芠aA���%�������81��/.�kI�|��۲G�Aݎ��k<�f����G�&�הh�d�;���Q�齍	c<斲'vI��	�6x$�w^l�(�1�	����n���3����Q�2��<��}�a�������d"q�\��r�kΙ&�Ι�%\���
F=�P"���p�����3O�k�4!E�(q��)����Jɥ���<����F����в�Ϣ�>�҅��~,��hO�X�q��$U�4C?���!!Èͽ6��?�x�y,_�7[:�+њ���)#_T$8#A�N�46�)l6���#��
cH��B���6/����{�������{.�Ta!,�l?���^��}نIO�+�SX�i��b1��Xke��w��V-K�Y1h$X�R����^�b6T����;{�}t'��CZ64�*7W�gd�B�P��{�-BڐPCw���ϟ/F�Ԉ�	4�P�Yh��(W��'`��&���������r�Kh�Yut,R:��&< ��O{FU!�t�U�:C�'�� ��l��:�چ�9/�x{%���*J�hc�8�@&i񡛂x��9d�kGV��JN��dV�`Dz�2)��2��q�2�IӜ���I�rR<�����T���|ַ��08���GXJ�)�_x�D������f���\bsõ+�!���nɧ&'�6���m�wa��KR�S8v�C��.�=zk�d�D�Q:Yq'7�ZL���R�x�FM�����oc����"C�.�M�B�4�>��e���3�I�����ͥز�L(;S��Pv�6Sʮ�cy�g�,���@�FAь�<s��<��u=b�f�!dt��+c@+��H���C@i
�M+��r� H�JQ1���܎# Ժp�M�3V�}f�A��`J���Q�-`��hHu8�s��)R�A�Z�f���=���%Y��"7e-�._�YD�j>���f��O�n�<L'�_����D:�>�`:@�pG�$��\�R�^�;�,ք��B����Ʌm��ݠ���N�[��sM* h��G�C,�
�7139�����x��	&}$j�mB�H��Ft���k9O�FdV;"ͤ�w~�[Ъ�N=����W��eU
}�S眫w�}�R��$���Z�Oz�4r�۶�P/8�t�<�q���
Hc�$��;�C[W��_�y���_�m|�)�D<��8�tq~�'�,�����>��j�Zќ/Eb=��˜�@~i���1r��v����_��[�̼��	H�G_��e/e�t�H�&YU�]��P�.u��>��;);��>J��T.H�^@���gHM�V�\Df �F��y�P[̦�����T� x_nSxAԧ�,ڸɹ�:���9Jzm�H�M���v�@�杦	��x�=�N���&�F����u�Gۆ���%j./1[�����w5;4u�V�+j��&Y�l�:r*���`s 7g�8g�v [�ɜvd9�fW�a_6h��ǭ��:r
��=&�*����PƁx�:����1��������υ\dt^��˘uI���g�5?�q�/5��O�MF?r���p�4I�ōd�B��B'�Ɔ��F] ��:{g�5[���D�-���jla �[�w6����d���N!��F���:9����rf*_տ@��XC��.�&i�l8+I{Q�3���|����4}YǗ�U�xi��Ȟ{��T�*]��e.���8�9:Jo�����7�"W��~)`H��<x���W�CE��J�U������x���Wz�W�Hm[�vҪL�����0&���
8@���Z���Ԙ��.~���j�I�獈��{S�X�쩌���QX֍0N��p�M��9KY�Q�Ï�B&6 x�Z��t�Rc��Fě����޽m�{P��q]z9�x�ߎX�o/k��eC]� �6��ަ��*v��?�"u�;��CޅZ����w�m�Qg����KxqS߂�F�I1���]34l2��{x���[@�ot���*YAǬ]S�A��/�g��R�t��� mk���Ν˂�h��l۾��>�I/XP���<<'j�*I���� �k<e%��#�|ٿ�C@{į�E�<�o=�3JhyZ�ӧ�����q$=�#H̔c��4%fn 0٭XCuU�����w��O��Y-c�37�"��py#�}�p��:���ȡ����M�*ѵ|����6*[]��Y�8o4���5q���}_��V{��reb<b>���9���=�"�K%�,���6����doF~����DL%��&����k2���v�G���]���&)r�S<��Y��=��L!j���u_�=e�=ag�����{���e�iY��0��yL��r?R�;Gy	����������?:TS�J��0���ۋd�n���bAwU$=/Zck���
lބ����.F��L9I�(4��S,�.�����ϭ�tӦ�'�or�l�	�Ǧe{� �읥z�����v0Կ}=���Z���]����9<��S�k�Q���V�g�$��:Ib��E��%��6F��5��终������y�G������%-��iӼ�8�l�m�{E���"oE��	�9��s���C�Ź��N"5h>Ÿ�@�����A�8���ӊ�:,��qJSb{�Y���Z`���GÕ5�:�6^���b�n�O�#0#�@�����LhP@0�;
������XL�Q[J�=��|w��v���&2y[���+V�!>f� ��u*߂߸*�� [��5Vb
�տ�
���)�d��G�7��9
��(4�]Nˍ�N��nԃ��l���{2�Cz��?�<�g*��k�B�d���E���*u�~�ҋ,C��ӎᔫ\[۬��]0�Ö�_�t o)���	�M�4��r�Ǝ�1��D��e�a�nX~L[٬�#�1�~(��\�\Q��6�>n�"��l���SX=�hu��6�T���p=#;����PZ$��2<�4�sw�0/�i���W�'ꎵ�5�"l?J�V!�R���}�L{򭿣�b47�C�`%B��½)[>7D��S[|�E����F�E�#�_iOK�]5������-؀>�މLKNv�ϊ�e�>�>ˏ�w�Cv̳�i�V�+m���h/�R{%�X��@{��X%~n��Dp	ST��<��݀\�h%�#�_�U!f	�/�{/Q�~G�_3�d�~��N1ۭ�\BT&�i�C4���@Tu�FA߅���V��d��-�7A����"t
'bO�˵���ݖ8K����l��l����S-3`��4�ɺw� =fY�ͱܔ�e�n�-�s�C�6�2e���|�+��t8�z�N����{+�8�r��1�=�ŀ6�wz�.�+H�m��u�v���#�Z��1�b��p����`@Z�w/�Q"f����!,G���Bn��_b=(�	Wȝ�-z��UνȌ/3:B��]Ρ�8"�w�(sk�dt<y*!6p�č��`[�+���S�e�h�H`�b�R��>A48�[S=���p�EmC�*OVZt��;�Od�4���;;��÷d�*�����?�K7�*�ä=q^���t�����h
M��;���q��8۳�i%k��
��OE9����b����7�􍛾�:��U�    ���̲�[]W�b܍����ֆf�t5��9[Li\�2M�(��@u��O��R�r����;t����]�, �t�&޲b[Z�w�S�Ȫu�,I|dN��m�ir�Dn�v�x_�?`�z����ScF+�˨�Xч ���QQ�%�Pb����j��Cp�t<����V��@��J��X%����뇡���$Y�@g�JO,����+j�B	�-V�j/�>|3�H�gŭ��.��Ğ%3uX���mOËU(�X(��e��(�xk�"�~�G�h{�T�7SY��t�,��J�L�V�'�ŉ��H���l޷y~��G���К�i�IY� ����
��Bq�7�W��fS/�R[�.:�r�!Зȭ���=��߮�r/_���b�\B��p�n��˧�S	�<�K�����20+�F�f��F:{(�ξ��Q����!��t�h�kW�~Y��-q
��;�r�v�K�'B#��Ys�|�S�����*�C^��@�E�3Z�Nˑ❚Q��h��6%g����F�Оb�\�O@��j�����=�[�!�#Z�Q�Sz�eQ�fů�����O[Ԅz�D�/q�u�$j]Q�KKN�7���ć��ȵ��^h�\g�6&��:8�
�[�ͽZ&�Y6j"ʥ��XÄd�^"b��}�m�=�2���|n(&�����EZ��Y-E��"�H�jj+Aע��ހ�0������'����V�{�3�r߈=-�hV0r��ޏ!�b29#�@_���g[�*��*&�ÖmiO��Cz�i�U���`���sx�-V�E��)�\-~-k�V�Ϣ�'��,�U���S�9ed�K���d���~�}��`*�c{|]A?�}Щ۰����3*��Pv�lâ�8Lڹ�~�<�=A3~�*���(IW���� w�h�Z�>k�eTO;��ݥ�����$�	��E;M��&^��� NBAr�9��5�c��X2Z.��ɚ�8_[8�������4�/�LM����6�-���_k���Qn�6j����ڰ�LQ�����2kĠ}�ފ����(��T��&l?����Y��9X}%)bÐ6Z��D��d����*��K�m��ft�nS�I�k,�ֿ��Z��b���*)P�����"K���_G���J�6!i��ERK{o Q.��z����N��ؼRG&rR4����H���ʖ �K6t��̈�p&GS�#��a$��+��gڴHn1��9+���/�g���2�W	ϫ�P�e�-�_[CP&��s��1P���@$��&��H�.���0�m�\� <�{�/o+�#�n9,D.8D�#��^f'�4^j����b;�2S�+M�
��WD+^�Ĭ�Dί$ 3��R¦�p����O������=�jpa�g��]�wl��ӎ���澾�l��IbꪭU��d���Xҗf��x�����YJ��X����~̣5ύ$���J�i���r����b��#A�5Z�hg�C4Y�k�TVݛ8OӁ��:����W�i ���V)_:9fp �fj���-�{�-�Dlj"v�䠵J�A�)U��P���>D�(XL��
a��؋K�iQjF�n�S��կC3��Ƿ��hah�s��,�ֲԢ$����m�Qљ�Y�N�w�Q�$�
0��O��C�=(V2��2r 1���#��ZTYNi�@6�罻x���E��hf���9*!���~OT�k�>���%�Gq���A�ۭw"���*��oR4��>h\�ER'�V���ࢊ��R��ME9ؠ�C!`�n\���	�"�s����q�����ʇ�1+��4�1*�4���}V�h2�}v}�08r&gw❃*i'GL$��V�x��ɩsop�!��h�6�<�ZC`���G^�����>�i�Y���k�m��-E�P�72���0T��`O����;���;�C��q6&y�I#� ��ʦ�����Wީ[}��ӧ��m ?��B���`�@|fo��)�g(=��3��#X�[�l,v������t����|u_�im[Iv"F8�s�l���#�&f���$"Ym������A�'�N�)�8�j�|���]���k�l�x�> ����b�o����_��~e��̎���{����h�>�q� �����&Mn�}��:;%�ʲ���(�Ш ����9��Pw���ݳ鼵!�{u3:�&)��p�F[�Yz��]��*F�6D�a��{��t����c�ًA6��xZ�|�􀦘ب�be���jf���O� �LB1BE4=�i� ��Zkݴ�U,f��Z��ZZł�m�{�c����䫢�I�5�Zi!�hk�	S:�ވ��*H�-�e�CжD]�ZL�ax�}�8��*R�uu6���b���E|����r����0_�nP���'�}%O����_�k�3ɮ��4S�l�ABZ�A�֬߰}BP�0j�J�&эJ�v�Қ�4^K�6,f}���У�O�qW��&Q(�9��-�4��^��EZ�e~��ɜ���^ H>�ዤ>
jئ�AN��7_f�F�<.i�5���"e �-�/�)/.W˿�=	�[� 9��OeQ\��Dh}������HB彾nn)L����N4-¤���, 'j��d-��QQ�Jԍu1u�������{�����r[�轷Y��u��Br/~d�d��D3g�x�b���F���l1i����Wn��m��똤��gy'g5R�@��Y>)V6��]m�A&^�J>|ƨz��f؋�1��k��ʑ�-)�̲���px�;��Ũ�̍��q/%��8��8Wa�
؛Ť�=P��io�/f��c}�1�5՞��)l���/�l6B��1[kM9�o`[:���한���ڪ�lr����[�l�3Pқt^�l�:�M�lFddD�հA9[� ����Kj���m4Oz���h| ������9:;r]-�g� l!K��F�>������w�o��
�!��B��?�<;�x�l.����e�}V�_���N4u���~#���Ϳ0At1AC�k��tvMI�&~Ǔ�t��D���C����C[f��$aѩY�8�ʱ��rp&\��A��H�k���,grr��2c����Y��2Io�2�' :}���a5�P�ڱ�&i�z�q˹+s' �/+��1w2M�'^��؟O�42�MJ�#ш?TJ�*\�������{�ޝ�����Ƿ
�@h_�D	r���<���n��QT4��@Ú�:�GI�#eMx=�I8/ JoE[����p>�.Q�Tב
\:�J;U,N��)�GW�_�z~U1�2x��ARC�S��2�&�v�@A�~���9�h��0T��Y�M�z4�)�@A+��� ��A�����(��3/���4�T�=�:�A�xe*�7�g���#�-Ĝ���u�d|øL(��*���Ԉ;�l#[�EJ}�hqm%��D@H��H�����|��8�'~_8f6����.}e�.�7���-�$��r������,\��H����
�u�;9Ѓ���!�cU��Q��:���5��膠
[k�1^v$G�����-�q�6�Y;�ɹ7FG�>�Oǋ�"�@�J@�D*
�������t�(Q�u�d��]�.��f�q`.36����]ܦ�P6��S2vJ�-&��&����c�͇���d�`�h�=����+�U��G�:�~@��Ӕ�oZfj��O�}�_�rE���؂J\[��8v�]�+�
7!�]$â�������{v���Jb{�:�c�5˔�oZ&L����)�h����b�vNv������|��r��T���?�je�ei;��s7��r��;_$�Vw��:�;@�/V�i��E��)�f �x�{���X3n�u���9���i�\��o�8R~(��qM�{�Y�[����٧�����+14r� �{�9��=^�S	p"]������ ]G$��˕t���kA��'YSJ���($�X}���#1��vTae����TvE(�oNr:�P��M�-�<2g��Q �ey �|�w� 	  ��9c5�o1���N��N�m��Y*Q��$����2|^�Z}+�Ŕ4�p�*A�#��a�0z����B��0�P0�2t�92M�Ӿ�l%��\$Yr�G,9���N�u��}��<����J�52���`{�{� #&I6���O�1��稂:l]��������m5
��,M�Z�-��jo��A	�ۓ����U����\ �ހ�`��K��i�Q�x��H��N����ⷈ���Kg<W�}�w�Q��͹a��\|�T�Q ��!pCQ4�����|<�zu3�񁇡h���;'ER����Wv�i#A���-� r��M֛�l^�^r�� �Q����U�3"$s#��n�tOOu�)�Qb�H�Z/Pҽu�Kh]8���(DA�/�M��l��&��/!�!_Z ���n�X|��D�v�]?��iT�7ʋ���nkķ@��w�2���	�֧�ް����f$v~Mvg-�?�$p^3��A��7Y�y%���dm��dMZ��F���	���{���;��K�K��V�U�Ȓ���=�o�k�j]��U�θ��8�Y��do-?��w`���@'�Z���9|9?b�Bg��RO��I�0}�Xga�|�p�f n��O ���aSq�ӭ?|�!�r����h����)�[>o�6NvѲM�E���KN�r+�V@W̊7�T�&Nc�f��sw���h5/d1]��\�Q����ޞo�=*��F6ǒJ7U���d�p������"%K7�a�ai���4D\�)y0D�P�Q��l�Z3�����#�NO�.s2L��$�z��ҿ���b/��<5����J27��(j[����2?�!��À+�0?��.ڸ�@(=9��5�[�o���v���|�'�"�XuF�m��F�a�,��;KW�߽%�C��F0�E+HhA��\�2	��-����T~�����2�Xt"�NnIk����� _���z̨:	�{�p�tQ����8�����Ŧ辫3���V���S��HT7�ʱ��F#x��?��'�lt�S��i��؄R�W�ñ���g�6���u�V�\�&k��َ�8 'L�T3��Y?��q����$�%�1W6���)x�;�*�wo<솽�66sht��Ͱ�����KY^�dk�x��Y*��xTy�Ak��7����e�7�6+g�d愝�b����U���;�m-Q���?���ݩ���{%gy7��3�-��$���3���H���*>�7�s�ɥ��=� ;~,Y&�R����KH��*q�*�췒������>��|�:���>H�����BB��/�ۤ�+��dD��U �������(�S�����?i����j�I"��/v�T����x���F���7 =g�����s|}�dJ��V�Y������T�^Y@,�����-����G��-gZSRB��j�Ϸ��7�v*������x�	h�7874�xt�wmGt�o�"֛��u����w-%!�;$�4D�I{k�xؿ>Q��!��42��a���\%�����iύb�)����i�iC��_55�]�@����L��ϲ��O|@(�-��R:�j�^wЖ���NR�,<�v����@�Q��mk�D�/���K�%�i��(���[G��aF����-�f�]ۥ,Q�����;�9ܓ�wv*%�ጦ仮��T�R���ڃ�+Bk�Ǒ�B ��v����36��GXAs�W'��<We���ʸjZk�T��޴�j�UҢa..%�Pr���6����\oɦ)���zw{/�Ʊ�t�
��*����=���2l�ɾ;�k�SQN�����n��G�����A�vT(A��v)���u�''NΗ����Oj���1��QT��u�F�'0�|�?��l2�����cUOv-JJ�1�h'Gď���wH��)z�-��a_U0	�M�@V<�?����n�6z� I:O=>���S1K�I�>k�%��T�Δá8�^�#;e"� �:w+C�bA�fh7`Q���L��a�i|�m���=@\������KNv+䥎��8�Dq
~։��Ѕ-�np���Cp�����I�D\v����f����u̎S�Ze�������<~��u���®����Sa�ǉ/~��I���Q���N���pM���>����j�Q��)���,{��[����q�i�,\Ui�8�<��Oю1M��wP6����7l^����`ܯ��~��ˋ���I�      �   7   x�Uʱ  �=Ǡ4h��;`A�hɢh�abx�.������]��.�0�QGH      �   {   x�3��K+ML�����yi�z����*F�*�F*9N�A�aE�f��n�9a�z��>Ω~��n&ii�n�.��š������YU��1~Pdd`d�kD�
��VF@d�g`hn�S�+F��� ��(�     